<?php

/**
 * @file
 * Hooks and so on for this.
 */

/**
 * React on project logs coming in to error for the first time because c2.
 */
function hook_violinist_projects_first_composer_2_required(\Drupal\violinist_projects\JobLog $log) {
  $nid = $log->getNid();
  \Drupal::logger('my_logger')->info('Seems that composer 2 is required for nid @nid now', [
    '@nid' => $nid,
  ]);
}

/**
 * React on a project coming in to error with php for a newish time.
 */
function hook_violinist_projects_first_php_too_low(\Drupal\violinist_projects\JobLog $log) {
  $nid = $log->getNid();
  \Drupal::logger('my_logger')->info('Seems that PHP is a bit low on @nid now', [
    '@nid' => $nid,
  ]);
}
