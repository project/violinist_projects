<?php

namespace Drupal\violinist_projects;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use violinist\JobSummary\JobSummary;

/**
 * Service to process jobs.
 */
class JobProcessor {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ETM.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a jobprocessor object.
   */
  public function __construct(StateInterface $state, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entityTypeManager) {
    $this->state = $state;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Method description.
   */
  public function processIncoming(JobLog $log) {
    $job_summary = new JobSummary($log->getStdOutLines());
    $this->processIncomingForUnupdate($job_summary, $log);
    $this->processIncomingForOldComposer($job_summary, $log);
    $this->processIncomingForOldPhp($job_summary, $log);
    $this->processIncomingForMaxConcurrent($job_summary, $log);
  }

  /**
   * See if this is new info about being too old php.
   */
  protected function processIncomingForOldPhp(JobSummary $job_summary, JobLog $log) {
    $errors = $job_summary->getErrorTypes();
    $node = $this->entityTypeManager->getStorage('node')
      ->load($log->getNid());
    if (!$node instanceof ProjectNode) {
      return;
    }
    $key = self::createStateKeyForType($log->getNid(), sprintf('php_too_low_%s', $node->getPhpVersion()));
    if ($job_summary->skippedForTimeFrame()) {
      // Nothing to see here I guess.
      return;
    }
    if (empty($errors)) {
      if ($job_summary->didFinishWithSuccess()) {
        // Then let's clear the key, as the thing might have stopped working,
        // and now it started working.
        $this->state->delete($key);
      }
      return;
    }
    $has_php_too_low_error = FALSE;
    foreach ($errors as $error) {
      if ($error !== JobSummary::PHP_NOT_SATISFIED) {
        continue;
      }
      $has_php_too_low_error = TRUE;
    }
    if (!$has_php_too_low_error) {
      // Then let's clear the key, as the thing might have stopped working, and
      // now it started working.
      $this->state->delete($key);
      return;
    }
    // Now see if this is the first time. Or a new time after it started
    // working again.
    if ($this->state->get($key)) {
      // No need to notify or whatever, several times?
      return;
    }
    $this->state->set($key, 1);
    $this->moduleHandler->invokeAll('violinist_projects_first_php_too_low', [
      $log,
    ]);
  }

  /**
   * Helper.
   *
   * See if this log was the first time this project used a too old composer
   * version, so we can react on that.
   */
  protected function processIncomingForOldComposer(JobSummary $job_summary, JobLog $log) {
    $errors = $job_summary->getErrorTypes();
    if (empty($errors)) {
      return;
    }
    $has_composer_2_required_error = FALSE;
    foreach ($errors as $error) {
      if ($error !== JobSummary::COMPOSER_2_REQUIRED_ERROR) {
        continue;
      }
      $has_composer_2_required_error = TRUE;
    }
    if (!$has_composer_2_required_error) {
      return;
    }
    // Now see if this is the first time.
    $key = self::createStateKeyComposer2Required($log->getNid());
    if ($this->state->get($key)) {
      // No need to notify or whatever, several times?
      return;
    }
    $this->state->set($key, 1);
    $this->moduleHandler->invokeAll('violinist_projects_first_composer_2_required', [
      $log,
    ]);
  }

  /**
   * Helper.
   *
   * If the fact that concurrent is reached is new, let's react on that.
   */
  protected function processIncomingForMaxConcurrent(JobSummary $job_summary, JobLog $log) {
    $has_max_concurrent_message = FALSE;
    $messages = $log->getStdOutLines();
    foreach ($messages as $message) {
      if (empty($message->type) || empty($message->message) || $message->type !== 'pr_exists') {
        continue;
      }
      // See if it matches a pattern over here yeah.
      if (!preg_match('/because the number of max concurrent PRs \(\d*\) seems to have been reached/', $message->message)) {
        continue;
      }
      $has_max_concurrent_message = TRUE;
    }
    $key = self::createStateKeyForType($log->getNid(), 'max_concurrent');
    if (!$has_max_concurrent_message) {
      // Make sure we delete the key, so we can send again when we encounter it
      // again.
      $this->state->delete($key);
      return;
    }
    if ($this->state->get($key)) {
      // No need to notify or whatever, several times?
      return;
    }
    $this->state->set($key, 1);
    $this->moduleHandler->invokeAll('violinist_projects_max_concurrent_encountered', [
      $log,
    ]);
  }

  /**
   * Helper.
   */
  protected function processIncomingForUnupdate(JobSummary $job_summary, JobLog $log) {
    $unupdated = $job_summary->getNotUpdated();
    foreach ($unupdated as $item) {
      if (empty($item->context->package)) {
        continue;
      }
      // Then there is the special case of "all". It will consist of the
      // following: The package name will be called simply "all", but it will
      // also contain a hash of the current updates attempted. So in practice:
      // all:ababefef.
      $package = $item->context->package;
      $is_all = FALSE;
      if (strpos($package, 'all:') === 0) {
        $is_all = TRUE;
        // This means we are expecting it to be a hash after a colon.
        $parts = explode(':', $package);
        if (count($parts) !== 2) {
          // Totally wrong format.
          continue;
        }
        // Let's create a state key based on this.
        $fake_item = (object) [
          'name' => $package,
          'version' => 'all',
          'latest' => 'all',
        ];
        $state_key = self::createStateKey($fake_item, $log->getNid());
        $safe_state_key = self::createStateKeyWithSafeLength($fake_item, $log->getNid());
        $update = $fake_item;
      }
      else {
        if (!$update = $job_summary->getUpdateForPackage($item->context->package)) {
          continue;
        }
        $state_key = self::createStateKey($update, $log->getNid());
        $safe_state_key = self::createStateKeyWithSafeLength($update, $log->getNid());
      }
      $value = $this->state->get($state_key);
      $value_to_set = time();
      if ($value) {
        // Already notified. Well, except if this is the all notification, we
        // want to notify every 7 days if that is the case.
        if (!$is_all) {
          continue;
        }
        // The value should be a timestamp. If that is more than a week old,
        // then we send another email.
        if ($value > time() - (3600 * 24 * 7)) {
          continue;
        }
      }
      $this->state->set($state_key, $value_to_set);
      $this->state->set($safe_state_key, $value_to_set);
      $this->moduleHandler->invokeAll('violinist_projects_new_unupdated', [
        $update,
        $log,
      ]);
    }
  }

  /**
   * Helper.
   */
  public static function createStateKey($update, $nid) {
    $key = sprintf('violinist_projects_%d_%s_%s_%s', $nid, $update->name, $update->version, $update->latest);
    // MySQL at least will have a maximum of 128 characters here, so if the key
    // is longer, we make it a bit shorter, I guess? The reason we are not
    // already just using the new safe length key is that we want to be able to
    // not simply start over, as that would instantly generate a duplicate
    // notification for those we have stored notifications for already. Guess we
    // could write some update hook here, but better to just store as both keys
    // (safe and unsafe) for now. And then remove the unsafe one later.
    if (strlen($key) >= 128) {
      $key = self::createStateKeyWithSafeLength($update, $nid);
    }
    return $key;
  }

  /**
   * Helper that makes sure we are under 128 characters.
   */
  public static function createStateKeyWithSafeLength($update, $nid) {
    return sprintf(
      'violinist_projects_%d_%s_%s_%s',
      $nid,
      substr(sha1($update->name), 0, 8),
      substr(sha1($update->version), 0, 8),
      substr(sha1($update->latest), 0, 8)
    );
  }

  /**
   * Helper.
   */
  public static function createStateKeyComposer2Required($nid) {
    return self::createStateKeyForType($nid, 'composer2_required');
  }

  /**
   * Helper here.
   */
  public static function createStateKeyForType($nid, string $type) {
    return sprintf('violinist_projects_%d_%s', $nid, $type);
  }

}
