<?php

namespace Drupal\violinist_projects;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * A service that creates job logs.
 */
class JobLogCreator {

  /**
   * The module handler to invoke the alter hook with.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a joblogcreator object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Method description.
   */
  public function createLogFromStringAndNid($string, $nid) {
    if (!$json = @json_decode($string)) {
      return FALSE;
    }
    if (empty($json->stdout)) {
      return FALSE;
    }
    $potential_json = '';
    $log = new JobLog();
    $log->setNid($nid);
    if (!empty($json->stderr)) {
      // Really should not be possible.
      $this->moduleHandler->invokeAll('violinist_projects_job_log_stderr', $json->stderr);
    }
    // Some times, the json is split over several lines. So we join them
    // together, and try to get json out of it.
    if (!empty($json->stdout[0])) {
      $potential_json = implode('', $json->stdout);
    }
    // But even before the json starts, there can be regular string lines. For
    // example if (god forbid) the script generated some notices or something
    // similar.
    $lines_removed = [];
    if (!$json_stdout = @json_decode($potential_json)) {
      // Try to remove one line at a time, until we can parse json.
      $lines = $json->stdout;
      while (count($lines) && !$json_stdout) {
        foreach ($lines as $delta => $line) {
          unset($lines[$delta]);
          $lines_removed[] = $line;
          $potential_json = implode('', $lines);
          if ($json_stdout = @json_decode($potential_json)) {
            break;
          }
        }
      }
    }
    if ($lines_removed) {
      $this->moduleHandler->invokeAll('violinist_projects_log_lines_removed', $lines_removed);
    }
    if (is_array($json_stdout)) {
      $log->setStdOutLines($json_stdout);
    }
    return $log;

  }

}
