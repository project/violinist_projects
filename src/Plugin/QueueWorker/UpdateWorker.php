<?php

namespace Drupal\violinist_projects\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\State\StateInterface;
use Drupal\deploy_key\KeyManager;
use Drupal\node\NodeInterface;
use Drupal\violinist_projects\Exception\NeedsUpdateException;
use Drupal\violinist_projects\NeedsUpdateManager;
use Drupal\violinist_projects\ProjectNode;
use Drupal\violinist_projects\TokenManager;
use Github\Exception\ApiLimitExceedException;
use Github\Exception\RuntimeException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Violinist\Slug\Slug;
use Violinist\UpdateCheckData\UpdateCheckData;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "violinist_check_project",
 *   title = @Translation("Check project updates")
 * )
 */
class UpdateWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  const LOG_TAG = 'violinist_projects';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Update manager.
   *
   * @var \Drupal\violinist_projects\NeedsUpdateManager
   */
  protected $updateManager;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Token manager.
   *
   * @var \Drupal\violinist_projects\TokenManager
   */
  protected $tokenManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Key manager.
   *
   * @var \Drupal\deploy_key\KeyManager
   */
  protected $keyManager;

  /**
   * UpdateWorker constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, NeedsUpdateManager $update_manager, StateInterface $state, TokenManager $token_manager, ModuleHandlerInterface $module_handler, LoggerInterface $logger, KeyManager $keyManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->updateManager = $update_manager;
    $this->state = $state;
    $this->tokenManager = $token_manager;
    $this->moduleHandler = $module_handler;
    $this->logger = $logger;
    $this->keyManager = $keyManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('violinist_projects.needs_update_manager'),
      $container->get('state'),
      $container->get('violinist_projects.token_manager'),
      $container->get('module_handler'),
      $container->get('logger.factory')->get(self::LOG_TAG),
      $container->get('deploy_key.key_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entityTypeManager->getStorage('node')->load($data);
    if (!$node) {
      return;
    }
    if (!$node instanceof ProjectNode) {
      return;
    }
    if ($node->isPaused()) {
      return;
    }
    $data = NULL;
    $daily_key = self::createDailyKey($node);
    $updater_key = 'violinist_projects_update_' . $node->id();
    // Check if its our daily run.
    $last_daily = $this->state->get($daily_key);
    $current_day = date('d.m.Y');
    $is_daily = ($current_day != $last_daily);
    if (!$user_token = $this->tokenManager->getTokenFromNode($node)) {
      return;
    }
    $data = $this->state->get($updater_key);
    if (!$data || !$data instanceof UpdateCheckData) {
      $data = new UpdateCheckData();
    }
    $start = microtime(TRUE);
    try {
      $providers = Slug::getSupportedProviders();
      $this->moduleHandler->alter('violinist_projects_supported_update_providers', $providers, $node);
      $slug = Slug::createFromUrlAndSupportedProviders($node->label(), $providers);
      $this->updateManager->setSlug($slug);
      $this->updateManager->setUserToken($user_token);
      $account = $this->entityTypeManager->getStorage('user')->load($node->getOwnerId());
      $this->updateManager->setProjectKey($this->keyManager->generateKeyForEntity($node));
      $this->updateManager->setUserKey($this->keyManager->generateKeyForEntity($account));
      $directory = NULL;
      if ($node->hasField('field_composer_json_path') && !$node->get('field_composer_json_path')->isEmpty()) {
        $directory = $node->get('field_composer_json_path')->first()->getString();
      }
      $env = [
        'COMPOSER_DISABLE_XDEBUG_WARN' => 1,
        'COMPOSER_ALLOW_SUPERUSER' => 1,
      ];
      $this->moduleHandler->alter('violinist_projects_show_command_env', $env, $node);
      $this->updateManager->checkNeedsUpdate($data, $env, $is_daily, $directory, $node);
    }
    catch (NeedsUpdateException $e) {
      $this->handleNeedsUpdate($node, $e);
    }
    catch (ApiLimitExceedException $e) {
      // Usually means that the user has revoked this access. Which means we
      // should not try to check the project any more?
      if ($node->hasField('field_pause_project')) {
        $this->logger->info('Caught ApiLimitExceededException. Pausing project @project with nid @nid', [
          '@project' => $node->label(),
          '@nid' => $node->id(),
        ]);
        $node->get('field_pause_project')->setValue(1);
        $node->save();
        $this->moduleHandler->invokeAll('violinist_projects_project_paused_in_update_worker', [
          $node,
          $e,
        ]);
      }
      return;
    }
    catch (RuntimeException $e) {
      if ($e->getCode() == 401 && $e->getMessage() === 'Bad credentials') {
        // Probably not much we can do...
        if ($node->hasField('field_pause_project')) {
          $this->logger->info('Caught RuntimeException. Pausing project @project with nid @nid', [
            '@project' => $node->label(),
            '@nid' => $node->id(),
          ]);
          $node->get('field_pause_project')->setValue(1);
          $node->save();
          $this->moduleHandler->invokeAll('violinist_projects_project_paused_in_update_worker', [
            $node,
            $e,
          ]);
        }
        return;
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Caught exception when checking for updates for node id @nid: @error', [
        '@error' => $e->getMessage(),
        '@nid' => $node->id(),
      ]);
      // This exception can happen if somehow there is something wrong with
      // something that fetches something. So for example if we are running this
      // queue item on a machine with a clock that is wrong, so it thinks a
      // bitbucket token has not expired. Or the token has expired, but we do
      // not have the secrets to regenerate it. Either way, always handle a
      // needs update for a daily run.
      if ($is_daily) {
        $exception = new NeedsUpdateException('Daily queue run');
        $this->handleNeedsUpdate($node, $exception);
      }
    }
    if ($data) {
      $this->state->set($updater_key, $data);
      if ($is_daily) {
        $this->state->set($daily_key, date('d.m.Y'));
      }
      $this->logger->info('Ran update check for nid @nid in @sec seconds', [
        '@nid' => $node->id(),
        '@sec' => microtime(TRUE) - $start,
      ]);
    }
  }

  /**
   * Helper to invoke the things needed from a typical needs update exception.
   */
  public function handleNeedsUpdate(NodeInterface $node, NeedsUpdateException $e) {
    $this->logger->info('Update needed for node id @id. Reason: @msg', [
      '@id' => $node->id(),
      '@msg' => $e->getMessage(),
    ]);
    $this->moduleHandler->invokeAll('violinist_projects_needs_update', [
      $node,
      $e,
    ]);
  }

  /**
   * Helper.
   */
  public static function createDailyKey(NodeInterface $node) {
    return 'violinist_projects_last_day_' . $node->id();
  }

}
