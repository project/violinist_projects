<?php

namespace Drupal\violinist_projects\Exception;

use Violinist\UpdateCheckData\UpdateCheckSha;

/**
 * Exception thrown to indicate an update being needed.
 */
class NeedsUpdateException extends \Exception {

  /**
   * Package that triggered this.
   *
   * @var string
   */
  private $package;

  /**
   * Sha that triggered this.
   *
   * @var \Violinist\UpdateCheckData\UpdateCheckSha
   */
  private $sha;

  /**
   * Getter.
   *
   * @return \Violinist\UpdateCheckData\UpdateCheckSha
   *   Sha.
   */
  public function getSha() {
    return $this->sha;
  }

  /**
   * Setter.
   *
   * @param \Violinist\UpdateCheckData\UpdateCheckSha $sha
   *   Sha.
   */
  public function setSha(UpdateCheckSha $sha) {
    $this->sha = $sha;
  }

  /**
   * Setter.
   */
  public function setPackage($package) {
    $this->package = $package;
  }

  /**
   * Getter.
   */
  public function getPackage() {
    return $this->package;
  }

}
