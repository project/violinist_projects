<?php

namespace Drupal\violinist_projects;

/**
 * The project run status value.
 */
enum ProjectRunStatusValue: string {
  case StatusNew = 'new';
  case StatusQueued = 'queued';
  case StatusRunning = 'running';
  case StatusProcessed = 'processed';
  case StatusErrored = 'errored';
  case StatusUnknown = 'unknown';
}
