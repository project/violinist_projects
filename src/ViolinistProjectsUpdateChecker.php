<?php

namespace Drupal\violinist_projects;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\node\NodeInterface;

/**
 * Service for checking projects and their update status.
 */
class ViolinistProjectsUpdateChecker {

  const QUEUE_NAME = 'violinist_check_project';

  const NODE_TYPE = 'project';

  /**
   * Node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a ViolinistProjectsUpdateChecker object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueueFactory $queue_factory, Connection $database) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->queue = $queue_factory->get(self::QUEUE_NAME);
    $this->database = $database;
  }

  /**
   * Queue all nodes for a check.
   */
  public function check() {
    $nids = $this->nodeStorage->getQuery()
      ->condition('type', self::NODE_TYPE)
      ->accessCheck(FALSE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->execute();
    // Also find nids in the queue.
    $query = $this->database->select('queue', 'q')
      ->fields('q', ['data']);
    $query->distinct();
    $data_nids = $query->execute();
    $queue_nids = [];
    foreach ($data_nids as $data_nid) {
      if (!$real_nid = @unserialize($data_nid->data)) {
        continue;
      }
      $queue_nids[] = $real_nid;
    }
    foreach ($nids as $nid) {
      if (in_array($nid, $queue_nids)) {
        continue;
      }
      $this->queue->createItem($nid);
    }
  }

}
