<?php

namespace Drupal\violinist_projects;

use Drupal\node\Entity\Node;
use Drupal\user\UserInterface;
use Drupal\violinist_teams\TeamNode;

/**
 * Class for our project nodes.
 */
class ProjectNode extends Node {

  const NOTIFICATIONS_DISABLED = 3;
  const ENABLED_WITH_OVERRIDE = 2;
  const TEAM_FIELD = 'field_team';
  const SLACK_NOTIFICATION_STATUS_NODE_FIELD = 'field_slack_notification_status';
  const SLACK_NOTIFICATION_ENABLED_USER_FIELD = 'field_slack_notifications';
  const PROJECT_FIELD = 'field_github_link';

  /**
   * Get the team affiliation of a project.
   *
   * @todo This should probably be required to return a team node at a point in
   * the future, when all projects are required to belong to a team.
   *
   * @return \Drupal\violinist_teams\TeamNode|null
   *   The team node, if found. Otherwise just null yeah.
   */
  public function getTeam() : ?TeamNode {
    // If the project is actually assigned to a team, we should be able to get
    // that.
    if (!$this->hasField(self::TEAM_FIELD)) {
      return NULL;
    }
    if ($this->get(self::TEAM_FIELD)->isEmpty()) {
      return NULL;
    }
    try {
      $all = $this->get(self::TEAM_FIELD)->referencedEntities();
      return reset($all);
    }
    catch (\Throwable $e) {
      return NULL;
    }
  }

  /**
   * Helper to wrap this for different fields.
   */
  public function getNotificationStatusForField($field) : int {
    if ($this->hasField($field) && !$this->get($field)->isEmpty()) {
      return (int) $this->get($field)->first()->getString();
    }
    // Default to disabled.
    return self::NOTIFICATIONS_DISABLED;
  }

  /**
   * Is the status for the project itself enabled or allowed to be overridden?
   */
  public function getProjectNotificationStatus() {
    return $this->getNotificationStatusForField('field_notification_status');
  }

  /**
   * Old getter for this.
   *
   * It was renamed because notifications can be enabled in more ways than one.
   *
   * @deprecated in violinist_projects:1.0.0 and is removed from
   * violinist_projects:2.0.0. Use isNotificationsEnabledForEmail() instead.
   *
   * @see https://www.drupal.org/project/violinist_projects/issues/3414137
   */
  public function isNotificationsEnabled() : bool {
    return $this->isNotificationsEnabledForEmail();
  }

  /**
   * Check if the status is enabled for slack.
   */
  public function isNotificationsEnabledForSlack() : bool {
    return $this->isNotificationsEnabledForField(self::SLACK_NOTIFICATION_STATUS_NODE_FIELD);
  }

  /**
   * Check if the status is enabled for field.
   *
   * This seems like a likely abstraction, but currently not used for more than
   * one field.
   */
  public function isNotificationsEnabledForField($field) : bool {
    $status = $this->getNotificationStatusForField($field);
    if ($status === self::NOTIFICATIONS_DISABLED) {
      // The project has disabled notifications on a project level.
      return FALSE;
    }
    if ($status === self::ENABLED_WITH_OVERRIDE) {
      return TRUE;
    }
    $user = $this->getOwner();
    if (!$user instanceof UserInterface) {
      return FALSE;
    }
    // @todo We should add teams support here, using the team global setting
    // here instead of the user, when that is supported.
    return self::getSlackNotificationStatusFromUser($user);
  }

  /**
   * Helper for the user object.
   */
  public static function getSlackNotificationStatusFromUser(UserInterface $user) : bool {
    if (!$user->hasField(self::SLACK_NOTIFICATION_ENABLED_USER_FIELD) || $user->get(self::SLACK_NOTIFICATION_ENABLED_USER_FIELD)->isEmpty()) {
      // No way to see what sort of settings they have.
      return FALSE;
    }
    return (bool) $user->get(self::SLACK_NOTIFICATION_ENABLED_USER_FIELD)->first()->getString();
  }

  /**
   * Is notifications enabled, somehow?
   *
   * Uses both the node itself, but also fallback global options.
   */
  public function isNotificationsEnabledForEmail() : bool {
    $status = $this->getProjectNotificationStatus();
    if ($status === self::NOTIFICATIONS_DISABLED) {
      // The project has disabled notifications on a project level.
      return FALSE;
    }
    if ($status === self::ENABLED_WITH_OVERRIDE) {
      // Enabled on the project level.
      return TRUE;
    }
    // First consult the team. Should probably always be a team, but let's keep
    // the fallback.
    $team = $this->getTeam();
    if ($team instanceof TeamNode) {
      if ($team->isEmailNotificationsOnFailedUpdatesEnabled()) {
        return TRUE;
      }
      return FALSE;
    }
    $user = $this->getOwner();
    if (!$user instanceof UserInterface) {
      return FALSE;
    }
    if (!$user->hasField('field_notifications_on_failed') || $user->get('field_notifications_on_failed')->isEmpty()) {
      // No way to see what sort of settings they have.
      return FALSE;
    }
    $enabled = (bool) $user->get('field_notifications_on_failed')->first()->getString();
    if (!$enabled) {
      return FALSE;
    }
    $mails_from_user = self::getUserNotificationEmails($user);
    if (!empty($mails_from_user)) {
      // They have emails there. That is something I guess.
      return TRUE;
    }
    // Default is false, this is totally opt-in.
    return FALSE;
  }

  /**
   * Get the emails from the user field, if needed.
   */
  public static function getUserNotificationEmails(UserInterface $user) {
    $mails = [];
    if (!$user->hasField('field_notification_emails') || $user->get('field_notification_emails')->isEmpty()) {
      return $mails;
    }
    $mails_string = $user->get('field_notification_emails')->first()->getString();
    return self::createMailArrayFromString($mails_string);
  }

  /**
   * Helper to make sure we only get somehow valid strings at least.
   */
  protected static function createMailArrayFromString(string $mails_string) : array {
    $mails = explode("\n", $mails_string);
    $mails = array_map('trim', $mails);
    return array_filter($mails);
  }

  /**
   * Get the emails from the actual node.
   */
  public function getProjectNotificationEmails() : array {
    $mails = [];
    if ($this->hasField('field_project_notification_mails') && !$this->get('field_project_notification_mails')->isEmpty()) {
      $mails_string = $this->get('field_project_notification_mails')->first()->getString();
      return self::createMailArrayFromString($mails_string);
    }
    return $mails;
  }

  /**
   * Get notification emails to use, uses fallback if necessary.
   */
  public function getNotificationEmails() : array {
    // First, let's see if they are even enabled?
    $mails = [];
    if (!$this->isNotificationsEnabledForEmail()) {
      return $mails;
    }
    // Now, let's see where we should get them from.
    $status = $this->getProjectNotificationStatus();
    if ($status === self::ENABLED_WITH_OVERRIDE) {
      // Make it possible to override the emails on a project level.
      $mails = array_merge($this->getProjectNotificationEmails());
    }
    if (empty($mails)) {
      // If we either did not find any override mails even if allowed, or the
      // project was simply not overridden, then we want to consult the email
      // settings of the team. If there is no team (well, theoretically should
      // not be possible any more), we consult the user.
      $team = $this->getTeam();
      if ($team instanceof TeamNode) {
        $mails = array_merge($team->getNotificationEmails());
      }
      else {
        $mails = array_merge(self::getUserNotificationEmails($this->getOwner()));
      }
    }
    return $mails;
  }

  /**
   * Check if the project is paused or not.
   */
  public function isPaused() : bool {
    $is_paused = FALSE;
    if ($this->hasField('field_pause_project') && !$this->get('field_pause_project')->isEmpty()) {
      $is_paused = (bool) $this->get('field_pause_project')->first()->getString();
    }
    return $is_paused;
  }

  /**
   * Increment the composer version.
   */
  public function incrementComposerVersion() : ProjectNode {
    $current_composer_version = $this->getComposerVersion();
    $max = 2;
    if ($current_composer_version >= $max) {
      return $this;
    }
    $new_composer_version = $current_composer_version + 1;
    $this->setComposerVersion($new_composer_version);
    return $this;
  }

  /**
   * Increment php version.
   */
  public function incrementPhpVersion() : ProjectNode {
    $current = $this->getPhpVersion();
    $matrix = [
      '7.0',
      '7.1',
      '7.2',
      '7.3',
      '7.4',
      '8.0',
      '8.1',
      '8.2',
      '8.3',
      '8.4',
    ];
    $current_key = array_search($current, $matrix);
    if ($current_key === FALSE) {
      throw new \InvalidArgumentException('The current PHP version is not in supported matrix');
    }
    // Then, increment by one.
    $new_key = $current_key + 1;
    if (empty($matrix[$new_key])) {
      throw new \InvalidArgumentException('The incremented PHP version is not in supported matrix');
    }
    $this->setPhpVersion($matrix[$new_key]);
    return $this;
  }

  /**
   * Setter for PHP version.
   */
  public function setPhpVersion(string $version) : ProjectNode {
    $this->set('field_php_version', $version);
    return $this;
  }

  /**
   * Helpful helper.
   */
  public function getPhpVersion() : string {
    if (!$this->hasField('field_php_version') || $this->get('field_php_version')->isEmpty()) {
      return '8.1';
    }
    return $this->get('field_php_version')->first()->getString();
  }

  /**
   * Set the composer version.
   *
   * @param int|string $new_version
   *   A new version to set.
   *
   * @return \Drupal\violinist_projects\ProjectNode
   *   The node you know.
   */
  public function setComposerVersion($new_version) : ProjectNode {
    $this->set('field_composer_version', $new_version);
    return $this;
  }

  /**
   * Get the composer version.
   */
  public function getComposerVersion() {
    $default = 2;
    if (!$this->hasField('field_composer_version')) {
      return $default;
    }
    if ($this->get('field_composer_version')->isEmpty()) {
      return $default;
    }
    $version = (string) $this->get('field_composer_version')->first()->getString();
    if (!$version) {
      return $default;
    }
    $allowed_versions = [
      '1',
      '2',
      '2.2',
    ];
    if (!in_array($version, $allowed_versions)) {
      return $default;
    }
    return $version;
  }

  /**
   * Get the project URL.
   */
  public function getProjectUrl() : string {
    // At some point in the future, we should add a field that is more generic
    // than this, and we should return that first, even actually live-migrating
    // it, how about that?
    if ($this->hasField(self::PROJECT_FIELD) && !$this->get(self::PROJECT_FIELD)->isEmpty()) {
      $value = $this->get('field_github_link')->first()->getValue();
      return $value["uri"];
    }
    // In theory, quite often the label of the node should actually match the
    // project URL. I don't know how this would end up out of sync, but at least
    // we have a fallback.
    return $this->label();
  }

  /**
   * Check if we should force private.
   *
   * Forcing private in this context means "private mode", meaning we should not
   * attempt to create a fork to run the updates in the project.
   */
  public function shouldForcePrivate() : bool {
    if ($this->hasField('field_force_private') && !$this->get('field_force_private')->isEmpty()) {
      return (bool) $this->get('field_force_private')->first()->getString();
    }
    return FALSE;
  }

}
