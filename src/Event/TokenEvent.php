<?php

namespace Drupal\violinist_projects\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Class used for the event.
 */
class TokenEvent extends Event {

  /**
   * The parsed url.
   *
   * @var array
   */
  protected $url;

  /**
   * Uid.
   *
   * @var int
   */
  protected $uid;

  /**
   * The token to use.
   *
   * @var string
   */
  protected $token;

  /**
   * TokenEvent constructor.
   */
  public function __construct($url, $uid, $token) {
    $this->url = $url;
    $this->uid = $uid;
    $this->token = $token;
  }

  /**
   * Get the token.
   *
   * @return string
   *   The current token.
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * Set the token.
   *
   * @param string $token
   *   New token.
   */
  public function setToken($token) {
    $this->token = $token;
  }

  /**
   * Get url.
   *
   * @return array
   *   URL parsed.
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * Get uid.
   *
   * @return int
   *   Uid.
   */
  public function getUid() {
    return $this->uid;
  }

}
