<?php

namespace Drupal\violinist_projects\Event;

use Drupal\violinist_projects\ProjectNode;

/**
 * Class used for the event.
 */
class TokenWithNodeEvent extends TokenEvent {

  /**
   * The node.
   *
   * @var \Drupal\violinist_projects\ProjectNode
   */
  protected ProjectNode $node;

  /**
   * TokenEvent constructor.
   */
  public function __construct($url, $uid, $token, ProjectNode $node) {
    parent::__construct($url, $uid, $token);
    $this->node = $node;
  }

  /**
   * Get the node.
   *
   * @return \Drupal\violinist_projects\ProjectNode
   *   The current node.
   */
  public function getNode() : ProjectNode {
    return $this->node;
  }

}
