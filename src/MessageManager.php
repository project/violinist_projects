<?php

namespace Drupal\violinist_projects;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\advancedqueue\Job;
use Drupal\node\NodeInterface;

/**
 * Message manager.
 */
class MessageManager {

  /**
   * Projects log.
   */
  const DATABASE_TABLE = 'violinist_projects_log';

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * MessageManager constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   */
  public function __construct(Connection $database, TimeInterface $time) {
    $this->database = $database;
    $this->time = $time;
  }

  /**
   * Get the last valid message of a node.
   */
  public function getLastValidMessage(NodeInterface $node) {
  }

  /**
   * Save a message.
   */
  public function saveMessage(NodeInterface $node, Job $job) {
    $this->database->insert(self::DATABASE_TABLE)
      ->fields([
        'nid' => $node->id(),
        'timestamp' => $this->time->getRequestTime(),
        'run_id' => $job->getId(),
      ])->execute();
  }

}
