<?php

namespace Drupal\violinist_projects;

use Violinist\Config\Config;

/**
 * Render config from composer.json.
 */
class ConfigRenderer {

  /**
   * Take JSON and get the config back.
   */
  public function createConfigFromComposerJson($json) {
    $return = [
      '#theme' => 'violinist_projects_config_status',
    ];
    $config = Config::createFromComposerData($json);
    if ($config->getBlockList()) {
      $return['#blocklist'] = $config->getBlockList();
    }
    // We want to inform people about this option that as been renamed.
    if (!empty($json->extra->violinist->blacklist)) {
      $return['#blacklist'] = 'set';
    }
    if ($config->getAssignees()) {
      $return['#assignees'] = $config->getAssignees();
    }
    $packages_with_bundles = $config->getPackagesWithBundles();
    if (!empty($packages_with_bundles)) {
      foreach ($packages_with_bundles as $package) {
        $return['#bundled_packages'][$package] = $config->getBundledPackagesForPackage($package);
      }
    }
    if ($config->hasConfigForKey('one_pull_request_per_package')) {
      $return['#one_pull_request_per_package'] = 0;
      if ($config->shouldUseOnePullRequestPerPackage()) {
        $return['#one_pull_request_per_package'] = 1;
      }
    }
    if ($config->getDefaultBranch()) {
      $return['#default_branch'] = $config->getDefaultBranch();
    }
    if ($config->hasConfigForKey('allow_updates_beyond_constraint')) {
      $return['#allow_updates_beyond_constraint'] = 0;
      if ($config->shouldAllowUpdatesBeyondConstraint()) {
        $return['#allow_updates_beyond_constraint'] = 1;
      }
    }
    if ($config->hasConfigForKey('update_with_dependencies')) {
      $return['#update_with_dependencies'] = 0;
      if ($config->shouldUpdateWithDependencies()) {
        $return['#update_with_dependencies'] = 1;
      }
    }
    if ($config->hasConfigForKey('update_dev_dependencies')) {
      $return['#update_dev_dependencies'] = 0;
      if ($config->shouldUpdateDevDependencies()) {
        $return['#update_dev_dependencies'] = 1;
      }
    }
    if ($config->hasConfigForKey('timezone')) {
      $return['#timezone'] = $config->getTimeZone();
    }
    if ($config->hasConfigForKey('timeframe_disallowed')) {
      $return['#timeframe_disallowed'] = $config->getTimeFrameDisallowed();
    }
    if ($config->hasConfigForKey('number_of_concurrent_updates')) {
      $return['#number_of_concurrent_updates'] = (int) $config->getNumberOfAllowedPrs();
    }
    if ($config->hasConfigForKey('security_updates_only')) {
      $return['#security_updates_only'] = 0;
      if ($config->shouldOnlyUpdateSecurityUpdates()) {
        $return['#security_updates_only'] = 1;
      }
    }
    if ($config->hasConfigForKey('run_scripts')) {
      $return['#run_scripts'] = 0;
      if ($config->shouldRunScripts()) {
        $return['#run_scripts'] = 1;
      }
    }
    if ($config->hasConfigForKey('commit_message_convention')) {
      $return['#commit_message_convention'] = $config->getCommitMessageConvention();
    }
    if ($config->hasConfigForKey('branch_prefix')) {
      $return['#branch_prefix'] = $config->getBranchPrefix();
    }
    if ($config->hasConfigForKey('allow_list')) {
      $return['#allow_list'] = $config->getAllowList();
    }
    if ($config->hasConfigForKey('allow_security_updates_on_concurrent_limit')) {
      $return['#allow_security_updates_on_concurrent_limit'] = (int) $config->shouldAllowSecurityUpdatesOnConcurrentLimit();
    }
    if ($config->hasConfigForKey('check_only_direct_dependencies')) {
      $return['#check_only_direct_dependencies'] = (int) $config->shouldCheckDirectOnly();
    }
    if ($config->hasConfigForKey('always_update_all')) {
      $return['#always_update_all'] = (int) $config->shouldAlwaysUpdateAll();
    }
    if ($config->hasConfigForKey('allow_update_indirect_with_direct')) {
      $return['#allow_update_indirect_with_direct'] = (int) $config->shouldUpdateIndirectWithDirect();
    }
    if ($config->hasConfigForKey('automerge')) {
      $return['#automerge'] = (int) $config->shouldAutoMerge();
    }
    if ($config->hasConfigForKey('automerge_security')) {
      $return['#automerge_security'] = (int) $config->shouldAutoMergeSecurity();
    }
    if ($config->hasConfigForKey('always_allow_direct_dependencies')) {
      $return['#always_allow_direct_dependencies'] = (int) $config->shouldAlwaysAllowDirect();
    }
    if ($config->hasConfigForKey('labels')) {
      $return['#labels'] = $config->getLabels();
    }
    if ($config->hasConfigForKey('labels_security')) {
      $return['#labels_security'] = $config->getLabelsSecurity();
    }

    return $return;
  }

}
