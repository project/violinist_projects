<?php

namespace Drupal\violinist_projects\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\violinist_projects\ComposerJsonFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Returns responses for Violinist projects routes.
 */
class ViolinistProjectsController extends ControllerBase {

  /**
   * Fetcher.
   *
   * @var \Drupal\violinist_projects\ComposerJsonFetcher
   */
  protected $fetcher;

  /**
   * ViolinistProjectsController constructor.
   */
  public function __construct(ComposerJsonFetcher $fetcher, AccountProxyInterface $current_user) {
    $this->fetcher = $fetcher;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('violinist_projects.composer_json_fetcher'),
      $container->get('current_user')
    );
  }

  /**
   * Builds the response.
   */
  public function build(Request $request) {
    if ($this->currentUser->isAnonymous()) {
      throw new AccessDeniedHttpException();
    }
    if (!$url = $request->get('url')) {
      throw new BadRequestHttpException();
    }
    try {
      $json = $this->fetcher->getComposerJsonFromRepo($url);
      return new JsonResponse($json);
    }
    catch (\Throwable $e) {
      return new JsonResponse('Sorry');
    }
  }

}
