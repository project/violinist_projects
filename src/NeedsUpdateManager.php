<?php

namespace Drupal\violinist_projects;

use Codeaken\SshKey\SshKey;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\vcs_provider_client\ClientFactory;
use Drupal\violinist_projects\Exception\NeedsUpdateException;
use Symfony\Component\Process\Process;
use Violinist\Slug\Slug;
use Violinist\UpdateCheckData\UpdateCheckData;
use Violinist\UpdateCheckData\UpdateCheckSha;

/**
 * The manager that decides if updates are needed.
 */
class NeedsUpdateManager {

  /**
   * User token.
   *
   * @var string
   */
  protected $userToken;

  /**
   * Slug.
   *
   * @var \Violinist\Slug\Slug
   */
  protected $slug;

  /**
   * User deploy key.
   *
   * @var \Codeaken\SshKey\SshKeyPair
   */
  protected $userKey;

  /**
   * Project key.
   *
   * @var \Codeaken\SshKey\SshKeyPair
   */
  protected $projectKey;

  /**
   * Client factory.
   *
   * @var \Drupal\vcs_provider_client\ClientFactory
   */
  protected $clientFactory;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Uuid service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $file;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a NeedsUpdateManager object.
   */
  public function __construct(ClientFactory $client_factory, ModuleHandlerInterface $module_handler, UuidInterface $uuid, FileSystemInterface $file, LoggerChannelFactoryInterface $logger_factory, CacheBackendInterface $cache) {
    $this->clientFactory = $client_factory;
    $this->moduleHandler = $module_handler;
    $this->uuid = $uuid;
    $this->file = $file;
    $this->loggerFactory = $logger_factory;
    $this->logger = $logger_factory->get('violinist_projects');
    $this->cache = $cache;
  }

  /**
   * Setter.
   */
  public function setUserKey($userKey) {
    $this->userKey = $userKey;
  }

  /**
   * Setter.
   */
  public function setProjectKey($projectKey) {
    $this->projectKey = $projectKey;
  }

  /**
   * Set the user token.
   *
   * @param mixed $userToken
   *   User token.
   */
  public function setUserToken($userToken) {
    $this->userToken = $userToken;
  }

  /**
   * Get the slug.
   *
   * @return mixed
   *   A slug.
   */
  public function getSlug() {
    return $this->slug;
  }

  /**
   * Set the slug.
   *
   * @param \Violinist\Slug\Slug $slug
   *   A slug.
   */
  public function setSlug(Slug $slug) {
    $this->slug = $slug;
  }

  /**
   * Checks for possible updates.
   *
   * @throws \Drupal\violinist_projects\Exception\NeedsUpdateException
   */
  public function checkNeedsUpdate(UpdateCheckData $data, array $env, $is_daily = FALSE, $directory = NULL, $node = NULL) {
    // First check the repo, so the composer.json gets updated.
    $url = $this->slug->getUrl();
    /** @var \Drupal\vcs_provider_client\ClientInterface $client */
    $client = $this->clientFactory->getClientFromUrl($url);
    if (!$client) {
      // No client found.
      return;
    }
    // Authenticate.
    $client->authenticate($this->userToken);
    $user = $this->slug->getUserName();
    $repo = $this->slug->getUserRepo();
    $file_to_fetch = 'composer.json';
    if ($directory) {
      $file_to_fetch = sprintf('%s/composer.json', $directory);
    }
    $composer_json = $client->getFileFromUrl($url, $user, $repo, $file_to_fetch);
    if (!$json = @json_decode($composer_json)) {
      throw new \Exception('Horrible composer.json data');
    }
    // Make sure we don't have this error with composer 2 (even if the project
    // may be running super smoothly on composer 1 for all we know):
    // require.vendor/Package is invalid, it should not contain uppercase
    // characters. Please use vendor/package instead.
    foreach (['require', 'require-dev'] as $key) {
      // Convert to string, replace with only lowercase characters, force back
      // in there.
      if (empty($json->{$key})) {
        continue;
      }
      $string = json_encode($json->{$key});
      $string = mb_strtolower($string);
      $json->{$key} = json_decode($string);
    }
    // With whatever overwritten thing we have done here, let's write it back to
    // the raw json string.
    $composer_json = json_encode($json);
    $auth_json = NULL;
    // Also see if we have an auth.json file.
    try {
      $file_to_fetch = 'auth.json';
      if ($directory) {
        $file_to_fetch = sprintf('%s/auth.json', $directory);
      }
      $auth = $client->getFileFromUrl($url, $user, $repo, $file_to_fetch);
      $auth_json = @json_decode($auth);
    }
    catch (\Exception $e) {
      // Ignore that for a while.
    }
    $this->moduleHandler->invokeAll('violinist_projects_composer_json_on_update', [
      $url,
      $json,
      $node,
    ]);
    // Now, if this is the daily run, just throw early.
    if ($is_daily) {
      throw new NeedsUpdateException('Daily queue run');
    }
    // First get the default branch.
    $default_branch = $client->getDefaultBranchFromUrl($url, $user, $repo);
    // Then find its sha.
    $current_sha = $client->getShaFromBranchAndUrl($url, $user, $repo, $default_branch);
    if (!$data->getLastSha() || $current_sha != $data->getLastSha()->getSha()) {
      // Needs update.
      $last_sha = 'unknown';
      $last_time = 'unknown';
      if ($data->getLastSha()) {
        $last_sha = $data->getLastSha()->getSha();
        $last_time = date('d.m.y H:i:s', $data->getLastSha()->getTimestamp());
      }
      $data->setLastSha(new UpdateCheckSha($current_sha, time()));
      throw new NeedsUpdateException("Current sha $current_sha is not the same as last sha $last_sha (from $last_time)");
    }
    foreach (['require', 'require-dev'] as $type) {
      if (!isset($json->{$type})) {
        continue;
      }
      foreach ($json->{$type} as $dep => $version) {
        $parts = explode('/', $dep);
        if (count($parts) === 1) {
          // This usually means it is a meta-package. Like ext-curl, or php. We
          // do not need to check those.
          continue;
        }
        if (!$sha = $data->getShaForPackage($dep)) {
          // For sure needs an update.
          if ($new_sha = $this->getNewShaForPackage($dep, $composer_json, $env, $auth_json)) {
            $this->logger->info('Current package sha for package @package is @sha', [
              '@package' => $dep,
              '@sha' => $new_sha,
            ]);
            $data->setShaForPackage($dep, new UpdateCheckSha($new_sha, time()));
            throw new NeedsUpdateException("No sha stored for package $dep");
          }
          $this->logger->warning('Sha for package @package was not stored, but also not found when looking for it. Thus it will be not found on next run as well. Hopefully we will be able to locate it then', [
            '@package' => $dep,
          ]);
          throw new NeedsUpdateException("No sha stored for package $dep");
        }
        $current_package_sha = $this->getNewShaForPackage($dep, $composer_json, $env, $auth_json);
        if (FALSE === $current_package_sha) {
          // Some error prevented us from finding the sha, We do not want to
          // store that, so we keep on checking other packages.
          continue;
        }
        $this->logger->info('Current package sha for package @package is @sha', [
          '@package' => $dep,
          '@sha' => $current_package_sha,
        ]);
        if ($sha->getSha() != $current_package_sha) {
          // Run update.
          $new_sha = new UpdateCheckSha($current_package_sha, time());
          $data->setShaForPackage($dep, $new_sha);
          $old_sha = $sha->getSha();
          $old_time = date('d.m.Y H:i:s', $sha->getTimestamp());
          $e = new NeedsUpdateException("Sha for package $dep ($current_package_sha) is different than sha we had stored ($old_sha from $old_time)");
          $e->setPackage($dep);
          $e->setSha($new_sha);
          throw $e;
        }
      }
    }
  }

  /**
   * Gets the computed sha for package data for a package.
   */
  public function getNewShaForPackage($package, $composer_json, array $env, $auth_json = NULL) {
    // We trust the fact that other packages that require the same name does not
    // do so from a completely different source.
    $cid = sprintf('violinist_project_cache_package_%s', md5($package));
    if ($data = $this->cache->get($cid)) {
      return $data->data;
    }
    // Run composer show in a directory where we have put the composer.json.
    $uuid = $this->uuid->generate();
    $directory = "/tmp/$uuid";
    $this->file->mkdir($directory);
    file_put_contents("$directory/composer.json", $composer_json);
    if ($auth_json) {
      file_put_contents("$directory/auth.json", json_encode($auth_json));
    }
    // @todo Should probably not assume this is the project structure.
    $project_root = DRUPAL_ROOT . '/..';
    $composer_path_suffix = '/vendor/bin/composer';
    $composer = $project_root . $composer_path_suffix;
    if (!file_exists($composer)) {
      // Try not being in the project root like that.
      $composer = DRUPAL_ROOT . $composer_path_suffix;
    }
    // If there are some path repositories, we want to at least place empty
    // files there, so composer does not crash.
    $composer_json_object = json_decode($composer_json);
    if (!empty($composer_json_object->repositories) && (is_array($composer_json_object->repositories) || $composer_json_object->repositories instanceof \stdClass)) {
      foreach ($composer_json_object->repositories as $repo) {
        if (empty($repo->url)) {
          continue;
        }
        if ((empty($repo->type) || $repo->type !== 'path')) {
          // If it starts with './' we consider it a path repo, meaning it's
          // relative to the composer root. Not sure any repos are defined like
          // this without also being defined as a path repo. But this was the
          // "old" logic, so let's keep that as well. Meaning that the "new"
          // logic is to consider repos with the type of "path" as a path repo.
          // Seems like the better logic, eh?
          if (strpos($repo->url, './') === FALSE) {
            continue;
          }
        }
        // It's totally supported to have wildcards in there. So for example, we
        // can have these alternatives as path repos:
        // custom/plugins/*
        // custom/*/plugins
        // */plugins/*
        // We should support all of them. But in this case, we can just replace
        // the asterisk with some string, and the wildcard will match exactly
        // one directory.
        $repo_url_suffix = $repo->url;
        $repo_url_suffix = str_replace('*', uniqid(), $repo_url_suffix);
        $new_dir = "$directory/$repo_url_suffix";
        $this->file->prepareDirectory($new_dir, FileSystemInterface::CREATE_DIRECTORY);
        file_put_contents("$new_dir/composer.json", json_encode([
          'name' => 'vendor/package',
        ]));
      }
    }
    if (!file_exists($composer)) {
      $this->logger->error('No composer found on path @path', [
        '@path' => $composer,
      ]);
      return FALSE;
    }

    $keys = array_map(function ($name) use ($directory) {
      /** @var \Codeaken\SshKey\SshKeyPair $key */
      $key = $this->{$name};
      if (!$key) {
        // That seems horrible.
        throw new \Exception('No key found on class for ' . $name);
      }
      $filename = sprintf("%s/%s", $directory, uniqid());
      $filename_pub = "$filename.pub";
      file_put_contents($filename, $key->getPrivateKey()->getKeyData(SshKey::FORMAT_PKCS8));
      chmod($filename, 0600);
      /** @var \Codeaken\SshKey\SshPublicKey $pub */
      $pub = $key->getPublicKey();
      file_put_contents($filename_pub, $pub->getKeyData(SshKey::FORMAT_OPENSSH));
      chmod($filename_pub, 0600);
      return [
        'key' => $key,
        'filename_private' => $filename,
        'filename_pub' => $filename_pub,
      ];
    }, ['userKey', 'projectKey']);
    // We also need to make sure these keys are available to us. Since we can
    // not rely on the ssh-agent, apparently (composer or git or something will
    // not honor them) we will instead generate a config file on the fly.
    $config_file = sprintf('%s/%s', $directory, uniqid());
    file_put_contents($config_file, sprintf('Host *
	IdentityFile %s

Host *
	IdentityFile %s

', $keys[0]['filename_private'], $keys[1]['filename_private']));
    $env['COMPOSER_HOME'] = $project_root;
    $env['GIT_SSH_COMMAND'] = "ssh -F $config_file";
    $process = new Process([
      $composer,
      "show",
      $package,
      "--all",
      "-d",
      $directory,
    ], NULL, $env);
    $process->setTimeout(30);
    $our_sha = FALSE;
    try {
      $start = microtime(TRUE);
      $process->run();
      $this->logger->info('Ran composer show for @package in @sec seconds', [
        '@package' => $package,
        '@sec' => microtime(TRUE) - $start,
      ]);
      $stdout = $process->getOutput();
      $stderr = $process->getErrorOutput();
      if (Settings::get('violinist_projects_debug', 0)) {
        $this->loggerFactory->get('violinist_projects_debug')->info('Package: @package. Stdout: @stdout. Stderr: @stderr', [
          '@package' => $package,
          '@stdout' => $stdout,
          '@stderr' => $stderr,
        ]);
      }
      // Theoretically, a package can be not found with composer show, but still
      // installable. This is because the lock file could contain specific
      // instructions, but the composer.json does not contain enough info to
      // find it. So we check for that output before we decide the package check
      // was not a failure.
      $storable_string = sprintf('Package %s not found', $package);
      $test_for_not_found_string = sprintf('%s in %s/composer.json', $storable_string, $directory);
      if (strpos($stderr, $test_for_not_found_string) !== FALSE) {
        // We use the storable string, otherwise the md5 would be different
        // every single time.
        $our_sha = md5($storable_string);
      }
      if (!$our_sha && empty($stdout)) {
        $this->logger->info('Package: @package. Stdout: @stdout. Stderr: @stderr', [
          '@package' => $package,
          '@stdout' => $stdout,
          '@stderr' => $stderr,
        ]);
        throw new \Exception(sprintf('Exit code from process checking update for %s was %d', $package, $process->getExitCode()));
      }
      $json_of_output = json_encode($stdout);
      $our_sha = $our_sha ? $our_sha : md5($json_of_output);
      // Now, let's see if we can parse it so we can actually use the commit
      // sha of the package.
      $lines = preg_split('/\r\n|\r|\n/', $stdout);
      foreach ($lines as $line) {
        if (strpos($line, 'source') === FALSE) {
          continue;
        }
        // Now split on space and use the last in the array, if it is of the
        // correct length.
        $cols = explode(' ', $line);
        if ($cols[4] == '[git]' && strlen($cols[count($cols) - 1]) === 40) {
          // Hopefully that is a git sha.
          $our_sha = $cols[count($cols) - 1];
          break;
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Caught exception for process: @msg', [
        '@msg' => $e->getMessage(),
      ]);
      if ($process) {
        $this->logger->info('Trying to stop process');
        $process_exit_code = $process->stop();
        if ($process_exit_code) {
          $this->logger->info('Process stop exit code: @code', [
            '@code' => $process_exit_code,
          ]);
        }
      }
    }
    // Clean up.
    $this->file->deleteRecursive($directory);
    if ($our_sha) {
      $this->cache->set($cid, $our_sha, time() + 600);
    }
    return $our_sha;
  }

}
