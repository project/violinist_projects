<?php

namespace Drupal\violinist_projects;

/**
 * Value object to hold job logs.
 */
class JobLog {

  /**
   * The lines from stdout.
   *
   * @var array
   */
  private $stdOutLines = [];

  /**
   * Nid.
   *
   * @var int
   */
  private $nid;

  /**
   * Get nid.
   *
   * @return int
   *   Nid.
   */
  public function getNid() {
    return $this->nid;
  }

  /**
   * Set nid.
   *
   * @param int $nid
   *   Nid.
   */
  public function setNid($nid) {
    $this->nid = $nid;
  }

  /**
   * Getter.
   *
   * @return array
   *   Lines.
   */
  public function getStdOutLines() {
    return $this->stdOutLines;
  }

  /**
   * Setter.
   *
   * @param array $stdOutLines
   *   Lines.
   */
  public function setStdOutLines(array $stdOutLines) {
    $this->stdOutLines = $stdOutLines;
  }

}
