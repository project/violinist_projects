<?php

namespace Drupal\violinist_projects;

/**
 * Events for this module.
 */
final class Events {

  const TOKEN_EVENT = 'violinists_projects.token_event';
  const TOKEN_WITH_NODE_EVENT = 'violinists_projects.token_with_node_event';

}
