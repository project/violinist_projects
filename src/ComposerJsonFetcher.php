<?php

namespace Drupal\violinist_projects;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\vcs_provider_client\ClientFactory;
use Drupal\vcs_provider_client\ClientInterface;
use Gitlab\Exception\RuntimeException;
use Violinist\Slug\Slug;

/**
 * Service to fetch composer.json files.
 */
class ComposerJsonFetcher {

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Token manger.
   *
   * @var \Drupal\violinist_projects\TokenManager
   */
  protected $tokenManager;

  /**
   * Client factory.
   *
   * @var \Drupal\vcs_provider_client\ClientFactory
   */
  protected $clientFactory;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ComposerJsonFetcher constructor.
   */
  public function __construct(AccountProxyInterface $current_user, TokenManager $tokenManager, ClientFactory $clientFactory, ModuleHandlerInterface $moduleHandler) {
    $this->currentUser = $current_user;
    $this->tokenManager = $tokenManager;
    $this->clientFactory = $clientFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Method description.
   */
  public function getComposerJsonFromRepo($url, $directory = NULL, $branch = NULL) {
    return $this->getComposerJsonFromRepoAndUser($url, $this->currentUser, $directory, $branch);
  }

  /**
   * Convenience function.
   */
  public function getComposerJsonFromRepoAndUser($url, AccountProxyInterface $user, $directory = NULL, $branch = NULL) {
    if (!$token = $this->tokenManager->getTokenFromUrlAndUid($url, $user->id())) {
      throw new \Exception('No token found for the user and that URL');
    }
    $providers = Slug::getSupportedProviders();
    $this->moduleHandler->alter('violinist_projects_supported_composer_json_providers', $providers, $this->currentUser);
    $slug = Slug::createFromUrlAndSupportedProviders($url, $providers);
    $client = $this->clientFactory->getClientFromUrl($url);
    try {
      $client->authenticate($token);
      return $this->doFetchFile($url, $slug, $client, $directory, $branch);
    }
    catch (RuntimeException $e) {
      // Could be a PAT.
      try {
        $client->authenticatePat($token);
        return $this->doFetchFile($url, $slug, $client, $directory);
      }
      catch (\Throwable $second_e) {
        // Re-throw the first exception.
        throw $e;
      }
    }
  }

  /**
   * Helper to run the actual file fetching.
   */
  protected function doFetchFile($url, Slug $slug, ClientInterface $client, $directory, $branch = NULL) {
    $file_to_fetch = 'composer.json';
    if ($directory) {
      $file_to_fetch = sprintf('%s/composer.json', $directory);
    }
    $contents = $client->getFileFromUrl($url, $slug->getUserName(), $slug->getUserRepo(), $file_to_fetch, $branch);
    if (!$json = @json_decode($contents)) {
      throw new \Exception('The JSON for composer.json was not JSON');
    }
    return $json;
  }

}
