<?php

namespace Drupal\violinist_projects;

use Drupal\league_oauth_login\Controller\LoginController;
use Drupal\league_oauth_login\LeagueOauthLoginPluginManager;
use Drupal\user\UserDataInterface;
use Drupal\violinist_projects\Event\TokenEvent;
use Drupal\violinist_projects\Event\TokenWithNodeEvent;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * A manager for tokens and how to get them.
 */
class TokenManager {
  use LoggerAwareTrait;

  /**
   * User data.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Login manager.
   *
   * @var \Drupal\league_oauth_login\LeagueOauthLoginPluginManager
   */
  protected $loginManager;

  /**
   * Constructs a TokenManager object.
   */
  public function __construct(UserDataInterface $user_data, EventDispatcherInterface $event_dispatcher, LeagueOauthLoginPluginManager $loginManager, LoggerInterface $logger) {
    $this->userData = $user_data;
    $this->eventDispatcher = $event_dispatcher;
    $this->loginManager = $loginManager;
    $this->setLogger($logger);
  }

  /**
   * Retrieves the last created node.
   */
  public function getTokenFromNode(ProjectNode $node) {
    $uid = $node->getOwnerId();
    $url = $node->getProjectUrl();
    $token_so_far = $this->getTokenFromUrlAndUid($url, $uid);
    // Now, make it possible to alter it based on the node as well.
    $event = new TokenWithNodeEvent($url, $uid, $token_so_far, $node);
    $this->eventDispatcher->dispatch($event, Events::TOKEN_WITH_NODE_EVENT);
    return $event->getToken();
  }

  /**
   * Get token from url and uid.
   */
  public function getTokenFromUrlAndUid($url, $uid) {
    $url = parse_url($url);
    $token = FALSE;
    switch ($url['host']) {
      case 'github.com':
        $token = $this->userData->get('league_oauth_login', $uid, 'github.token');
        break;

      case 'gitlab.com':
        $token = $this->userData->get('league_oauth_login', $uid, 'gitlab.token');
        try {
          if ($serialized = $this->userData->get('league_oauth_login', $uid, 'gitlab.token.serialized')) {
            $attempted_refreshed_token = $this->ensureFreshToken($serialized, 'gitlab');
            LoginController::saveUserDataKeyWithDataAndTokenForUid($this->userData, $uid, LoginController::createUserDataKeyFromPluginId('gitlab'), $attempted_refreshed_token);
            $token = $attempted_refreshed_token->getToken();
          }
        }
        catch (\Throwable $e) {
          // Not sure what could be the problem here?
          $this->logger->error('Caught an exception trying to figure out the gitlab token. The message was @msg and the trace was @trace', [
            '@msg' => $e->getMessage(),
            '@trace' => $e->getTraceAsString(),
          ]);
        }
        break;

      case 'bitbucket.org':
        $token = $this->userData->get('league_oauth_login', $uid, 'bitbucket.token');
        try {
          if ($serialized = $this->userData->get('league_oauth_login', $uid, 'bitbucket.token.serialized')) {
            $attempted_refreshed_token = $this->ensureFreshToken($serialized, 'bitbucket');
            LoginController::saveUserDataKeyWithDataAndTokenForUid($this->userData, $uid, LoginController::createUserDataKeyFromPluginId('bitbucket'), $attempted_refreshed_token);
            $token = $attempted_refreshed_token->getToken();
          }
        }
        catch (\Throwable $e) {
          $this->logger->error('Caught an exception trying to figure out the bitbucket token. The message was @msg and the trace was @trace', [
            '@msg' => $e->getMessage(),
            '@trace' => $e->getTraceAsString(),
          ]);
        }
        break;
    }
    // Make it possible to alter it.
    $event = new TokenEvent($url, $uid, $token);
    $this->eventDispatcher->dispatch($event, Events::TOKEN_EVENT);
    return $event->getToken();
  }

  /**
   * Helper to make sure the token we operate on is fresh.
   *
   * @param \League\OAuth2\Client\Token\AccessToken $access_token
   *   A stored access token.
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return \League\OAuth2\Client\Token\AccessToken
   *   The token, either the very same or a refreshed one.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function ensureFreshToken(AccessToken $access_token, string $plugin_id) : AccessToken {
    if (!$access_token->getExpires() || !$access_token->hasExpired()) {
      return $access_token;
    }
    /** @var \Drupal\league_oauth_login\LeagueOauthLoginInterface $plugin */
    $plugin = $this->loginManager->createInstance($plugin_id);
    return $plugin->getProvider()->getAccessToken('refresh_token', [
      'refresh_token' => $access_token->getRefreshToken(),
    ]);
  }

}
