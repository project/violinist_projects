<?php

namespace Drupal\violinist_projects;

use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * A service to retrieve, store and manage run status for projects.
 */
class ProjectRunStatus {

  use StringTranslationTrait;

  const STATE_KEY_PREFIX = 'violinist_projects.run_status';

  public function __construct(
    private readonly StateInterface $state,
    TranslationInterface $translation,
  ) {
    $this->stringTranslation = $translation;
  }

  /**
   * Setter for the run status for a project.
   */
  public function setRunStatusForProject(ProjectNode $node, $status) {
    $key = self::getStateKey($node);
    if (!$status instanceof ProjectRunStatusValue) {
      // Let's fail silently here.
      return;
    }
    $this->state->set($key, $status);
  }

  /**
   * Get a human readable version of the status for a project.
   *
   * To be used in the interface, for example.
   */
  public function getHumanReadableStatusForProject(ProjectNode $node) {
    $status = $this->getStatusForProject($node);
    return match ($status) {
      ProjectRunStatusValue::StatusNew => $this->t('New'),
      ProjectRunStatusValue::StatusQueued => $this->t('Queued'),
      ProjectRunStatusValue::StatusRunning => $this->t('Running'),
      ProjectRunStatusValue::StatusProcessed => $this->t('Processed'),
      ProjectRunStatusValue::StatusErrored => $this->t('Errored'),
      default => 'Unknown',
    };
  }

  /**
   * Gets the actual status for a project.
   */
  public function getStatusForProject(ProjectNode $node) : ProjectRunStatusValue {
    $key = self::getStateKey($node);
    $stored_value = $this->state->get($key, ProjectRunStatusValue::StatusUnknown);
    if ($stored_value instanceof ProjectRunStatusValue) {
      return $stored_value;
    }
    $enum_value = ProjectRunStatusValue::tryFrom($stored_value);
    if (!$enum_value) {
      return ProjectRunStatusValue::StatusUnknown;
    }
    return $enum_value;
  }

  /**
   * Get the state key for a project.
   */
  public static function getStateKey(ProjectNode $node) {
    return sprintf('%s.%s', self::STATE_KEY_PREFIX, $node->id());
  }

}
