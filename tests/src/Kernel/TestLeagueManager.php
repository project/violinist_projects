<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\league_oauth_login\LeagueOauthLoginPluginManager;

/**
 * A manager we can use to return overridden things in tests.
 */
class TestLeagueManager extends LeagueOauthLoginPluginManager {

  /**
   * Constructs LeagueOauthLoginPluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct($namespaces, $cache_backend, $module_handler);
    $this->alterInfo('league_oauth_login_info');
    $this->setCacheBackend($cache_backend, 'league_oauth_login_info');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return new DummyInstance();
  }

}
