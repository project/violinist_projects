<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the config renderer.
 *
 * @group violinist_projects
 */
class ConfigRendererTest extends KernelTestBase {

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Config renderer.
   *
   * @var \Drupal\violinist_projects\ConfigRenderer|object|null
   */
  protected $configRenderer;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'violinist_projects',
    'vcs_provider_client',
    'deploy_key',
    'user',
    'league_oauth_login',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->renderer = $this->container->get('renderer');
    $this->configRenderer = $this->container->get('violinist_projects.config_renderer');
  }

  /**
   * All variations of empty should give this result.
   *
   * @dataProvider getEmptyVariations
   */
  public function testRenderEmpty($json) {
    $html = $this->createHtmlFromComposerData($json);
    $html_string = (string) $html;
    self::assertHtmlMatchesHtml('You have not configured an allow list for this project.', $html_string);
    self::assertHtmlMatchesHtml('<h3>Allow security updates on concurrent limit <a title="What does this mean?" href="https://docs.violinist.io/configuration/allow_security_updates_on_concurrent_limit/"><i class="fa fa-question-circle"></i></a></h3> <div class="allow-security-updates-on-concurrent-limit"> No configuration was found. This means your project will use the default value, which is <code>0</code>.', $html_string);
    self::assertHtmlMatchesHtml('<div class="allow-update-indirect-with-direct">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>.', $html_string);
    self::assertHtmlMatchesHtml('<div class="allow-updates-beyond-constraint">
                            No configuration was found. This means your project will use the default value, which is <code>1</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="always-update-all">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>', $html_string);
    self::assertHtmlMatchesHtml('You have not configured any assignees for this project.', $html_string);
    self::assertHtmlMatchesHtml('<div class="automerge">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="automerge-security">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>', $html_string);
    self::assertHtmlMatchesHtml('You have not configured a block list for this project.', $html_string);
    self::assertHtmlMatchesHtml('<div class="branch-prefix">
                            No configuration was found. This means your project will use the default value, which is <code>&quot;&quot;</code> (empty string).', $html_string);
    self::assertHtmlMatchesHtml('You have not configured any bundled packages.', $html_string);
    self::assertHtmlMatchesHtml('<div class="check-only-direct-dependencies">
                            No configuration was found. This means your project will use the default value, which is <code>1</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="commit-message-convention">
      No configuration was found. This means your project will use the default, which is <code>""</code> (empty string). An empty string indicates no convention, and is the same as setting this to <code>none</code>.', $html_string);
    self::assertHtmlMatchesHtml('No configuration was found. This means your project will use the default branch of your project.', $html_string);
    self::assertHtmlMatchesHtml('<div class="number-of-concurrent-updates">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="one-pull-request-per-package">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="run-scripts">
                            No configuration was found. This means your project will use the default value, which is <code>1</code>.', $html_string);
    self::assertHtmlMatchesHtml('<div class="security-updates-only">
                            No configuration was found. This means your project will use the default value, which is <code>0</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="timeframe-disallowed">
                            No configuration was found. This means your project will use the default value, which is <code>0</code> (meaning no timeframe is disallowed).', $html_string);
    self::assertHtmlMatchesHtml('<div class="timezone">
                            No configuration was found. This means your project will use the default value, which is <code>+0000</code> (meaning timezone will be UTC+0000).', $html_string);
    self::assertHtmlMatchesHtml('<div class="update-dev-dependencies">
                            No configuration was found. This means your project will use the default value, which is <code>1</code>.', $html_string);
    self::assertHtmlMatchesHtml('<div class="update-with-dependencies">
                            No configuration was found. This means your project will use the default value, which is <code>1</code>', $html_string);
    self::assertHtmlMatchesHtml('<div class="always-allow-direct-dependencies">
                    No configuration was found. This means your project will use the default value, which is <code>0</code>.
            </div>', $html_string);
    self::assertHtmlMatchesHtml('<h3>Labels <a title="What does this mean?" href="https://docs.violinist.io/#labels"><i class="fa fa-question-circle"></i></a></h3>
<div class="labels">
            You have not configured any labels for this project.
        </div>', $html_string);
    self::assertHtmlMatchesHtml('<h3>Labels on security updates <a title="What does this mean?" href="https://docs.violinist.io/#labels_security"><i class="fa fa-question-circle"></i></a></h3>
<div class="labels-security">
            You have not configured any security labels for this project.
        </div>', $html_string);
  }

  /**
   * A data provider.
   */
  public static function getEmptyVariations() {
    return [
      [
        (object) [
          'extra' => NULL,
        ],
      ],
      [
        (object) [],
      ],
      [
        (object) [
          'extra' => (object) [
            'violinist' => 'test',
          ],
        ],
      ],
      [
        (object) [
          'extra' => (object) [
            'violinist' => [],
          ],
        ],
      ],
      [
        (object) [
          'extra' => (object) [
            'violinist' => (object) [],
          ],
        ],
      ],
      [
        (object) [
          'extra' => (object) [
            'violinist' => [
              // Just one of these arrays that can be empty and still show the
              // same message in the HTML.
              'blocklist' => [],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Test that allow list thing is in there as expected.
   */
  public function testAllowList() {
    $html = $this->createHtmlFromConfig([
      'allow_list' => [
        'vendor/package',
      ],
    ]);
    self::assertHtmlMatchesHtml('<h3>Allow list <a title="What does this mean?" href="https://docs.violinist.io/#allow-list"><i class="fa fa-question-circle"></i></a></h3>
    <div class="allow-list">
              <ul>
                      <li>vendor/package</li>
                  </ul>
                </div>', (string) $html);
    // A stupid format should not allow it.
    $html = $this->createHtmlFromConfig([
      'allow_list' => 'derp',
    ]);
    self::assertHtmlMatchesHtml('You have not configured an allow list for this project.', (string) $html);
  }

  /**
   * Test allow security updates on concurrent limit.
   *
   * @dataProvider provideBooleanValues
   */
  public function testAllowSecurityUpdatesOnConcurrentLimit($value) {
    $html = $this->createHtmlFromConfig([
      'allow_security_updates_on_concurrent_limit' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Allow security updates on concurrent limit <a title="What does this mean?" href="https://docs.violinist.io/configuration/allow_security_updates_on_concurrent_limit/"><i class="fa fa-question-circle"></i></a></h3>
    <div class="allow-security-updates-on-concurrent-limit">
              Configuration value: <code>%s</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test that we show that blacklist is deprecated.
   */
  public function testDeprecatedBlockList() {
    $html = $this->createHtmlFromConfig([
      'blacklist' => [
        'vendor/package',
      ],
    ]);
    self::assertHtmlMatchesHtml('You are using a block list with the option <code>blacklist</code>, which has been deprecated. Please switch to using <code>blocklist</code> instead. Your configuration is still being used, and should be listed below under "block list".', (string) $html);
  }

  /**
   * Test different variations of commit message convention.
   *
   * @dataProvider getCommitMessageVariations
   */
  public function testCommitMessageConvention($value, $expected_result) {
    $html = $this->createHtmlFromConfig([
      'commit_message_convention' => $value,
    ]);
    self::assertHtmlMatchesHtml($expected_result, (string) $html);
  }

  /**
   * Dataprovider.
   */
  public static function getCommitMessageVariations() {
    return [
      [
        'none',
        '<h3>Commit message convention <a title="What does this mean?" href="https://docs.violinist.io/#commit_message_convention"><i class="fa fa-question-circle"></i></a></h3>
    <div class="commit-message-convention">
          Configuration value: <code>"none"</code>.',
      ],
      [
        'some',
        '<h3>Commit message convention <a title="What does this mean?" href="https://docs.violinist.io/#commit_message_convention"><i class="fa fa-question-circle"></i></a></h3>
    <div class="commit-message-convention">
          Configuration value: <code>"some"</code>.',
      ],
      [
        '',
        '<h3>Commit message convention <a title="What does this mean?" href="https://docs.violinist.io/#commit_message_convention"><i class="fa fa-question-circle"></i></a></h3>
    <div class="commit-message-convention">
          Configuration value: <code>""</code>.
              An empty string indicates no convention will be used, and is the same as setting this to <code>none</code>.',
      ],
    ];
  }

  /**
   * Test the block list option.
   *
   * It is not deprecated.
   */
  public function testNonDeprecatedBlockList() {
    $html = $this->createHtmlFromConfig([
      'blocklist' => [
        'vendor/package',
      ],
    ]);
    self::assertHtmlNotMatchesHtml('You are using a block list with the option <code>blacklist</code>, which has been deprecated. Please switch to using <code>blocklist</code> instead. Your configuration is still being used, and should be listed below under "block list".', (string) $html);
    self::assertHtmlMatchesHtml('<h3>Block list <a title="What does this mean?" href="https://docs.violinist.io/#blocklisting-projects"><i class="fa fa-question-circle"></i></a></h3>
    <div class="blocklist">
              <ul>
                      <li>vendor/package</li>
                  </ul>
                </div>', (string) $html);
  }

  /**
   * Test the labels.
   */
  public function testLabels() {
    $html = $this->createHtmlFromConfig([
      'labels' => [
        'label1',
      ],
    ]);
    self::assertHtmlMatchesHtml('<h3>Labels <a title="What does this mean?" href="https://docs.violinist.io/#labels"><i class="fa fa-question-circle"></i></a></h3>
<div class="labels">
            <ul>
              <li>label1</li>
            </ul>
        </div>', (string) $html);
  }

  /**
   * Test the labels.
   */
  public function testLabelsSecurity() {
    $html = $this->createHtmlFromConfig([
      'labels_security' => [
        'label1',
      ],
    ]);
    self::assertHtmlMatchesHtml('<h3>Labels on security updates <a title="What does this mean?" href="https://docs.violinist.io/#labels_security"><i class="fa fa-question-circle"></i></a></h3>
<div class="labels-security">
            <ul>
              <li>label1</li>
            </ul>
        </div>', (string) $html);
  }

  /**
   * Test assignees.
   */
  public function testAssignees() {
    $html = $this->createHtmlFromConfig([
      'assignees' => [
        'assignee_user_1',
      ],
    ]);
    self::assertHtmlMatchesHtml('<h3>Assignees <a title="What does this mean?" href="https://docs.violinist.io/#assignees"><i class="fa fa-question-circle"></i></a></h3>
    <div class="assignees">
              <ul>
                      <li>assignee_user_1</li>
                  </ul>
                </div>', (string) $html);
  }

  /**
   * Test bundled packages.
   */
  public function testBundledPackages() {
    $html = $this->createHtmlFromConfig([
      'bundled_packages' => (object) [
        'vendor/mypackage' => [
          'vendor/other_package',
          'vendor/third_package',
        ],
      ],
    ]);
    self::assertHtmlMatchesHtml('<h3>Bundled packages <a title="What does this mean?" href="https://docs.violinist.io/#bundled-packages"><i class="fa fa-question-circle"></i></a></h3>
    <div class="bundled-packages">
            <p>
        <strong>vendor/mypackage</strong>. Updates are bundled with:
        <ul>
                  <li>vendor/other_package</li>
                  <li>vendor/third_package</li>
                </ul>
        </p>', $html);
  }

  /**
   * Test direct only.
   *
   * @dataProvider provideBooleanValues
   */
  public function testDirectOnly($value) {
    $html = $this->createHtmlFromConfig([
      'check_only_direct_dependencies' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Check only direct dependencies <a title="What does this mean?" href="https://docs.violinist.io/#check-only-direct"><i class="fa fa-question-circle"></i></a></h3>
    <div class="check-only-direct-dependencies">
              Configuration value: <code>%d</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test auto merge.
   *
   * @dataProvider provideBooleanValues
   */
  public function testAutoMerge($value) {
    $html = $this->createHtmlFromConfig([
      'automerge' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Auto merge <a title="What does this mean?" href="https://docs.violinist.io/#automerge"><i class="fa fa-question-circle"></i></a></h3>
    <div class="automerge">
              Configuration value: <code>%s</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test auto merge security.
   *
   * @dataProvider provideBooleanValues
   */
  public function testAutoMergeSecurity($value) {
    $html = $this->createHtmlFromConfig([
      'automerge_security' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Auto merge security updates <a title="What does this mean?" href="https://docs.violinist.io/#automerge_security"><i class="fa fa-question-circle"></i></a></h3>
    <div class="automerge-security">
              Configuration value: <code>%s</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test direct with indirect.
   *
   * @dataProvider provideBooleanValues
   */
  public function testDirectWithIndirect($value) {
    $html = $this->createHtmlFromConfig([
      'allow_update_indirect_with_direct' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Allow update direct with only dependencies <a title="What does this mean?" href="https://docs.violinist.io/#allow_update_direct_with_only_dependencies"><i class="fa fa-question-circle"></i></a></h3>
    <div class="allow-update-indirect-with-direct">
              Configuration value: <code>%s</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test one PR per package.
   *
   * @dataProvider provideBooleanValues
   */
  public function testOnePr($value) {
    $html = $this->createHtmlFromConfig([
      'one_pull_request_per_package' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>One pull request per dependency <a title="What does this mean?" href="https://docs.violinist.io/#one-pull-request-per-package"><i class="fa fa-question-circle"></i></a></h3>
    <div class="one-pull-request-per-package">
              Configuration value: <code>%d</code>', $value), (string) $html);
  }

  /**
   * Test default branch.
   */
  public function testDefaultBranch() {
    $html = $this->createHtmlFromConfig([
      'default_branch' => 'develop',
    ]);
    self::assertHtmlMatchesHtml('<h3>Default branch <a title="What does this mean?" href="https://docs.violinist.io/#default_branch"><i class="fa fa-question-circle"></i></a></h3>
    <div class="default-branch">
              Configuration value: <code>develop</code>
          </div>', (string) $html);
  }

  /**
   * Test default branch.
   */
  public function testBranchPrefix() {
    $html = $this->createHtmlFromConfig([
      'branch_prefix' => 'violinist-prefix-',
    ]);
    self::assertHtmlMatchesHtml('<h3>Branch prefix <a title="What does this mean?" href="https://docs.violinist.io/#branch-prefix"><i class="fa fa-question-circle"></i></a></h3>
    <div class="branch-prefix">
              Configuration value: <code>violinist-prefix-</code>', (string) $html);
  }

  /**
   * Test the option allow_updates_beyond_constraint.
   *
   * @dataProvider provideBooleanValues
   */
  public function testAllowBeyond($value) {
    $html = $this->createHtmlFromConfig([
      'allow_updates_beyond_constraint' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Allow update beyond version constraint <a title="What does this mean?" href="https://docs.violinist.io/#updating-beyond"><i class="fa fa-question-circle"></i></a></h3>
    <div class="allow-updates-beyond-constraint">
              Configuration value: <code>%d</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test the option allow_updates_beyond_constraint.
   *
   * @dataProvider provideBooleanValues
   */
  public function testUpdateAll($value) {
    $html = $this->createHtmlFromConfig([
      'always_update_all' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Always update all <a title="What does this mean?" href="https://docs.violinist.io/#updating-all"><i class="fa fa-question-circle"></i></a></h3>
    <div class="always-update-all">
              Configuration value: <code>%d</code>
          </div>', $value), (string) $html);
  }

  /**
   * Test the option always_allow_direct_dependencies.
   *
   * @dataProvider provideBooleanValues
   */
  public function testAlwaysAllowDirect($value) {
    $html = $this->createHtmlFromConfig([
      'always_allow_direct_dependencies' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Always allow direct dependencies <a title="What does this mean?" href="https://docs.violinist.io/#always-allow-direct"><i class="fa fa-question-circle"></i></a></h3>
<div class="always-allow-direct-dependencies">
            Configuration value: <code>%s</code>
      </div>', $value), (string) $html);
  }

  /**
   * Test the option update_with_dependencies.
   *
   * @dataProvider provideBooleanValues
   */
  public function testUpdateWith($value) {
    $html = $this->createHtmlFromConfig([
      'update_with_dependencies' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Update with dependencies <a title="What does this mean?" href="https://docs.violinist.io/#update-with-deps"><i class="fa fa-question-circle"></i></a></h3>
    <div class="update-with-dependencies">
              Configuration value: <code>%d</code>', $value), (string) $html);
  }

  /**
   * Tet update_dev_dependencies.
   *
   * @dataProvider provideBooleanValues
   */
  public function testUpdateDev($value) {
    $html = $this->createHtmlFromConfig([
      'update_dev_dependencies' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Update dev dependencies <a title="What does this mean?" href="https://docs.violinist.io/#update_dev_dependencies"><i class="fa fa-question-circle"></i></a></h3>
    <div class="update-dev-dependencies">
              Configuration value: <code>%d</code>', $value), (string) $html);
  }

  /**
   * Tet timezone.
   */
  public function testTimezone() {
    $html = $this->createHtmlFromConfig([
      'timezone' => '+0200',
    ]);
    self::assertHtmlMatchesHtml('<h3>Timezone <a title="What does this mean?" href="https://docs.violinist.io/#timezone"><i class="fa fa-question-circle"></i></a></h3>
    <div class="timezone">
              Configuration value: <code>+0200</code>', (string) $html);
  }

  /**
   * Test timeframe.
   */
  public function testTimeframe() {
    $html = $this->createHtmlFromConfig([
      'timeframe_disallowed' => '0600-1800',
    ]);
    self::assertHtmlMatchesHtml('<h3>Timeframe disallowed <a title="What does this mean?" href="https://docs.violinist.io/#timeframe_disallowed"><i class="fa fa-question-circle"></i></a></h3>
    <div class="timeframe-disallowed">
              Configuration value: <code>0600-1800</code>', (string) $html);
  }

  /**
   * Test number_of_concurrent_updates.
   */
  public function testConcurrent() {
    $html = $this->createHtmlFromConfig([
      'number_of_concurrent_updates' => '5',
    ]);
    self::assertHtmlMatchesHtml('<h3>Number of concurrent updates <a title="What does this mean?" href="https://docs.violinist.io/#number-of-concurrent-updates"><i class="fa fa-question-circle"></i></a></h3>
    <div class="number-of-concurrent-updates">
              Configuration value: <code>5</code>', (string) $html);
  }

  /**
   * Test security_updates_only.
   *
   * @dataProvider provideBooleanValues
   */
  public function testSecOnly($value) {
    $html = $this->createHtmlFromConfig([
      'security_updates_only' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Security updates only <a title="What does this mean?" href="https://docs.violinist.io/#security-updates-only"><i class="fa fa-question-circle"></i></a></h3>
    <div class="security-updates-only">
              Configuration value: <code>%d</code>', $value), (string) $html);
  }

  /**
   * Test run_scripts.
   *
   * @dataProvider provideBooleanValues
   */
  public function testRunScripts($value) {
    $html = $this->createHtmlFromConfig([
      'run_scripts' => $value,
    ]);
    self::assertHtmlMatchesHtml(sprintf('<h3>Run scripts <a title="What does this mean?" href="https://docs.violinist.io/#run-scripts"><i class="fa fa-question-circle"></i></a></h3>
    <div class="run-scripts">
              Configuration value: <code>%d</code>', $value), (string) $html);
  }

  /**
   * Helper for the violinist part in extra to html.
   */
  protected function createHtmlFromConfig(array $config = []) {
    $json = (object) [
      'extra' => (object) [
        'violinist' => (object) $config,
      ],
    ];
    return $this->createHtmlFromComposerData($json);
  }

  /**
   * Common dataprovider.
   */
  public static function provideBooleanValues() {
    return [
      [0],
      [1],
    ];
  }

  /**
   * Helper for composer.json (object) to html.
   */
  protected function createHtmlFromComposerData($json) {
    $data = $this->configRenderer->createConfigFromComposerJson($json);
    return (string) $this->renderer->renderRoot($data);
  }

  /**
   * Helper to ignore whitespace when asserting HTML contents.
   */
  public static function assertHtmlMatchesHtml($expected, $actual) {
    $expected = preg_replace('/\s+/', ' ', $expected);
    $actual = preg_replace('/\s+/', ' ', $actual);
    self::assertStringContainsString($expected, $actual);
  }

  /**
   * Helper to ignore whitespace when asserting HTML contents.
   */
  public static function assertHtmlNotMatchesHtml($expected, $actual) {
    $expected = preg_replace('/\s+/', ' ', $expected);
    $actual = preg_replace('/\s+/', ' ', $actual);
    self::assertStringNotContainsString($expected, $actual);
  }

}
