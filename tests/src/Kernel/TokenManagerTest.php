<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\user\Entity\User;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Test that the token manager does what we think it does.
 *
 * @group violinist_projects
 */
class TokenManagerTest extends ViolinistProjectsTestBase implements ServiceModifierInterface {

  const TEST_TOKEN = 'test-token';
  const TEST_TOKEN_UPDATED = 'test-token-updated';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login',
    'league_oauth_login_bitbucket',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
  }

  /**
   * Test the different things with the token manager.
   *
   * @dataProvider tokenManagerProvider
   */
  public function testTokenManager($url) {
    /** @var \Drupal\violinist_projects\TokenManager $service */
    $service = $this->container->get('violinist_projects.token_manager');
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    // Set it for all known providers.
    $user_data = $this->container->get('user.data');
    $user_data->set('league_oauth_login', $user->id(), 'github.token', self::TEST_TOKEN);
    $user_data->set('league_oauth_login', $user->id(), 'gitlab.token', self::TEST_TOKEN);
    $user_data->set('league_oauth_login', $user->id(), 'bitbucket.token', self::TEST_TOKEN);
    $token = $service->getTokenFromUrlAndUid($url, $user->id());
    self::assertEquals(self::TEST_TOKEN, $token);
    // Now let's also test with a serialized token. Not expired.
    $test_token = new AccessToken([
      'access_token' => self::TEST_TOKEN,
      'expires' => time() + 1000,
    ]);
    $user_data->set('league_oauth_login', $user->id(), 'github.token.serialized', $test_token);
    $user_data->set('league_oauth_login', $user->id(), 'gitlab.token.serialized', $test_token);
    $user_data->set('league_oauth_login', $user->id(), 'bitbucket.token.serialized', $test_token);
    $token = $service->getTokenFromUrlAndUid($url, $user->id());
    self::assertEquals(self::TEST_TOKEN, $token);
    // Now let's test an expired one. Well, except github, which does not
    // expire.
    if ($url === 'https://github.com/test/test') {
      return;
    }
    $test_token = new AccessToken([
      'access_token' => self::TEST_TOKEN,
      'expires' => time() - 1000,
    ]);
    $user_data->set('league_oauth_login', $user->id(), 'github.token.serialized', $test_token);
    $user_data->set('league_oauth_login', $user->id(), 'gitlab.token.serialized', $test_token);
    $user_data->set('league_oauth_login', $user->id(), 'bitbucket.token.serialized', $test_token);
    $token = $service->getTokenFromUrlAndUid($url, $user->id());
    self::assertEquals(self::TEST_TOKEN_UPDATED, $token);
  }

  /**
   * Data provider for our test.
   */
  public static function tokenManagerProvider() {
    return [
      ['https://github.com/test/test'],
      ['https://bitbucket.org/test/test'],
      ['https://gitlab.com/test/test'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('plugin.manager.league_oauth_login')
      ->setClass(TestLeagueManager::class);
  }

}
