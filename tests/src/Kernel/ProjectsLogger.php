<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\Core\Logger\LoggerChannel;

/**
 * A logger to hold events.
 */
class ProjectsLogger extends LoggerChannel {

  /**
   * Events.
   *
   * @var array
   */
  private $events = [];

  /**
   * {@inheritdoc}
   */
  public function log($level, \Stringable|string $message, array $context = []): void {
    $this->events[] = [
      $level,
      $message,
      $context,
    ];
  }

  /**
   * Get everything we logged.
   */
  public function getEvents() {
    return $this->events;
  }

}
