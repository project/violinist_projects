<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use League\OAuth2\Client\Token\AccessToken;

/**
 * Technically not even a plugin instance, but has the method we need.
 */
class DummyInstance {

  /**
   * The getter for the provider.
   *
   * We just use the same class to avoid creating more files, heh.
   */
  public function getProvider() {
    return $this;
  }

  /**
   * Theoretically a method on the provider class.
   */
  public function getAccessToken($type, $parameters) {
    return new AccessToken([
      'access_token' => TokenManagerTest::TEST_TOKEN_UPDATED,
    ]);
  }

}
