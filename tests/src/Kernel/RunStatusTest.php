<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\node\Entity\Node;
use Drupal\violinist_projects\ProjectNode;
use Drupal\violinist_projects\ProjectRunStatus;
use Drupal\violinist_projects\ProjectRunStatusValue;

/**
 * Test the run status service.
 *
 * @group violinist_projects
 */
class RunStatusTest extends ViolinistProjectsTestBase {

  /**
   * The project node.
   */
  private ProjectNode $projectNode;

  /**
   * The project run status service.
   */
  private ProjectRunStatus $projectRunStatus;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->projectRunStatus = $this->container->get('violinist_projects.run_status');
    $this->projectNode = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'Test project',
    ]);
    $this->projectNode->save();
  }

  /**
   * Test the human readable status.
   *
   * @dataProvider statusProviderWithHumanReadable
   */
  public function testHumanReadable($status, $expected) {
    $this->projectRunStatus->setRunStatusForProject($this->projectNode, $status);
    $this->assertEquals((string) $expected, $this->projectRunStatus->getHumanReadableStatusForProject($this->projectNode));
  }

  /**
   * Test the status.
   *
   * @dataProvider statusProvider
   */
  public function testStatus($status, $expected) {
    $this->projectRunStatus->setRunStatusForProject($this->projectNode, $status);
    $this->assertEquals($expected, $this->projectRunStatus->getStatusForProject($this->projectNode));
  }

  /**
   * Dataprovider for the status test.
   */
  public static function statusProvider() {
    return [
      [
        ProjectRunStatusValue::StatusNew,
        ProjectRunStatusValue::StatusNew,
      ],
      [
        ProjectRunStatusValue::StatusQueued,
        ProjectRunStatusValue::StatusQueued,
      ],
      [
        ProjectRunStatusValue::StatusRunning,
        ProjectRunStatusValue::StatusRunning,
      ],
      [
        ProjectRunStatusValue::StatusProcessed,
        ProjectRunStatusValue::StatusProcessed,
      ],
      [
        ProjectRunStatusValue::StatusErrored,
        ProjectRunStatusValue::StatusErrored,
      ],
      [
        ProjectRunStatusValue::StatusUnknown,
        ProjectRunStatusValue::StatusUnknown,
      ],
      [
        NULL,
        ProjectRunStatusValue::StatusUnknown,
      ],
      [
        'something else',
        ProjectRunStatusValue::StatusUnknown,
      ],
    ];
  }

  /**
   * Dataprovider for the human readable test.
   */
  public static function statusProviderWithHumanReadable() {
    return [
      [
        ProjectRunStatusValue::StatusNew,
        'New',
      ],
      [
        ProjectRunStatusValue::StatusQueued,
        'Queued',
      ],
      [
        ProjectRunStatusValue::StatusRunning,
        'Running',
      ],
      [
        ProjectRunStatusValue::StatusProcessed,
        'Processed',
      ],
      [
        ProjectRunStatusValue::StatusErrored,
        'Errored',
      ],
      [
        ProjectRunStatusValue::StatusUnknown,
        'Unknown',
      ],
      [
        'something else',
        'Unknown',
      ],
      [
        NULL,
        'Unknown',
      ],
    ];
  }

}
