<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\node\Entity\Node;

/**
 * Test the job processor.
 *
 * @group violinist_projects
 */
class JobProcessorTest extends ViolinistProjectsTestBase implements ServiceModifierInterface {

  /**
   * Job processor.
   *
   * @var \Drupal\violinist_projects\JobProcessor
   */
  protected $processor;

  /**
   * Log creator.
   *
   * @var \Drupal\violinist_projects\JobLogCreator
   */
  protected $logCreator;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'violinist_projects_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->processor = $this->container->get('violinist_projects.job_processor');
    $this->logCreator = $this->container->get('violinist_projects.job_log_creator');
  }

  /**
   * Test that things happen as we expect when we encounter composer 2 required.
   */
  public function testComposer2Required() {
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'my project',
    ]);
    $node->save();
    $log_string = [
      'stdout' => [
        json_encode([
          [
            // Just faking a message that kind of looks like the composer one,
            // but still at least matches the regex.
            'message' => 'Plugin api exists as composer-plugin-api[1.0.0] but these are no good',
            'type' => 'message',
          ],
        ]),
      ],
    ];
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $state = $this->container->get('state');
    self::assertCount(0, $state->get(VIOLINIST_PROJECTS_TEST_COMPOSER_2_REQUIRED_HOOK_CALLED_KEY, []));
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_COMPOSER_2_REQUIRED_HOOK_CALLED_KEY, []));
    // Now create another job log, and see if we process this twice.
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_COMPOSER_2_REQUIRED_HOOK_CALLED_KEY, []));
  }

  /**
   * Test that things happen as we expect when we encounter composer 2 required.
   */
  public function testPhpVersionTooLow() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'my project',
    ]);
    $node->save();
    $log_string = [
      'stdout' => [
        json_encode([
          [
            'message' => 'Your lock file does not contain a compatible set of packages. Please run composer update.

  Problem 1
    - Root composer.json requires php >=8.0 but your php version (7.4.26) does not satisfy that requirement.',
            'type' => 'message',
          ],
        ]),
      ],
    ];
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $state = $this->container->get('state');
    self::assertCount(0, $state->get(VIOLINIST_PROJECTS_TEST_PHP_TOO_LOW_HOOK_CALLED_KEY, []));
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_PHP_TOO_LOW_HOOK_CALLED_KEY, []));
    // Now create another job log, and see if we process this twice.
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_PHP_TOO_LOW_HOOK_CALLED_KEY, []));
    // But of course, if we kind of clear it out with a successful run, then we
    // should call it again.
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode([
      'stdout' => [
        json_encode([
          [
            'message' => 'Cleaning up after update check.',
            'type' => 'no type',
          ],
        ]),
      ],
    ]), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_PHP_TOO_LOW_HOOK_CALLED_KEY, []));
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(2, $state->get(VIOLINIST_PROJECTS_TEST_PHP_TOO_LOW_HOOK_CALLED_KEY, []));
    // Of course, if we change the PHP version, it should trigger again.
    $node->incrementPhpVersion()->save();
    $this->processor->processIncoming($job_log);
    self::assertCount(3, $state->get(VIOLINIST_PROJECTS_TEST_PHP_TOO_LOW_HOOK_CALLED_KEY, []));
  }

  /**
   * Test that things happen as we expect when we encounter unupdated.
   */
  public function testUnupdated() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'my project',
    ]);
    $node->save();
    $log_string = [
      'stdout' => [
        json_encode([
          [
            'message' => 'package vendor/mypackage was not updated running composer update',
            'type' => 'notupdated',
            'context' => (object) [
              'package' => 'all:ababfefef',
            ],
          ],
        ]),
      ],
    ];
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $state = $this->container->get('state');
    self::assertCount(0, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    // Now create another job log, and see if we process this twice.
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    // If there are new commits, it should increment again.
    $log_string = [
      'stdout' => [
        json_encode([
          [
            'message' => 'package vendor/mypackage was not updated running composer update',
            'type' => 'notupdated',
            'context' => (object) [
              'package' => 'all:fefeabab',
            ],
          ],
        ]),
      ],
    ];
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(2, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    // And it should not trigger again.
    $this->processor->processIncoming($job_log);
    self::assertCount(2, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    // Now set the state key to more than a week in the past. This should
    // trigger another hook invocation.
    $state_key = 'violinist_projects_1_all:fefeabab_all_all';
    $state->set($state_key, time() - (3600 * 24 * 8));
    $this->processor->processIncoming($job_log);
    self::assertCount(3, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    // But not a second time.
    $this->processor->processIncoming($job_log);
    self::assertCount(3, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
  }

  /**
   * Test that things happen as we expect when we encounter pr skipped.
   *
   * I mean skipped because of max concurrent reached.
   */
  public function testMaxConcurrent() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'my project',
    ]);
    $node->save();
    $log_string = [
      'stdout' => [
        json_encode([
          [
            'message' => 'Skipping composer/composer because the number of max concurrent PRs (15) seems to have been reached',
            'type' => 'pr_exists',
            'context' => (object) [
              'package' => 'composer/composer',
            ],
          ],
        ]),
      ],
    ];
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $state = $this->container->get('state');
    self::assertCount(0, $state->get(VIOLINIST_PROJECTS_TEST_MAX_CONCURRENT_HOOK_CALLED_KEY, []));
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_MAX_CONCURRENT_HOOK_CALLED_KEY, []));
    // Now create another job log, and see if we process this twice.
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_MAX_CONCURRENT_HOOK_CALLED_KEY, []));
    // Now do a successful one, and see it made possible to trigger again.
    $log_string2 = [
      'stdout' => [
        json_encode([]),
      ],
    ];
    $job_log2 = $this->logCreator->createLogFromStringAndNid(json_encode($log_string2), $node->id());
    $this->processor->processIncoming($job_log2);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_MAX_CONCURRENT_HOOK_CALLED_KEY, []));
    // And it should now trigger again.
    $this->processor->processIncoming($job_log);
    self::assertCount(2, $state->get(VIOLINIST_PROJECTS_TEST_MAX_CONCURRENT_HOOK_CALLED_KEY, []));
    // But not even one more time.
    $this->processor->processIncoming($job_log);
    self::assertCount(2, $state->get(VIOLINIST_PROJECTS_TEST_MAX_CONCURRENT_HOOK_CALLED_KEY, []));
  }

  /**
   * Test that dependencies that are really long and really short all work.
   */
  public function testStateKeyReallyLong() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'my project',
    ]);
    $node->save();
    $log_string = [
      'stdout' => [
        json_encode([
          [
            'message' => 'The following updates were found:',
            'type' => 'update',
            'context' => (object) [
              'packages' => [
                (object) [
                  'name' => 'drupal/pretty_long_name',
                  'version' => 'dev-1234567-drupal-10-compatibility ababfefef',
                  'latest' => 'dev-1234567-drupal-10-compatibility efefabab',
                ],
              ],
            ],
          ],
          [
            'message' => 'package drupal/pretty_long_name was not updated running composer update',
            'type' => 'notupdated',
            'context' => (object) [
              'package' => 'drupal/pretty_long_name',
            ],
          ],
        ]),
      ],
    ];
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $state = $this->container->get('state');
    self::assertCount(0, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
    // Now create another job log, and see if we process this twice.
    $job_log = $this->logCreator->createLogFromStringAndNid(json_encode($log_string), $node->id());
    $this->processor->processIncoming($job_log);
    self::assertCount(1, $state->get(VIOLINIST_PROJECTS_TEST_UNUPDATED_HOOK_CALLED_KEY, []));
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->set('keyvalue', new KeyValueFactory($container));
  }

}
