<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Codeaken\SshKey\SshKeyPair;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Update manger test cases for Kernel tests.
 *
 * @group violinist_projects
 */
class UpdateManagerTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'violinist_projects',
    'vcs_provider_client',
    'deploy_key',
    'user',
    'league_oauth_login',
  ];

  /**
   * Test the method that gets the sha.
   */
  public function testGetSha() {
    /** @var \Drupal\violinist_projects\NeedsUpdateManager $mng */
    $mng = $this->container->get('violinist_projects.needs_update_manager');
    $composer_json = json_encode([
      'require' => [
        'psr/log' => '1.0.0',
      ],
    ]);
    $key = SshKeyPair::generate();
    $mng->setUserKey($key);
    $mng->setProjectKey($key);
    $this->setSetting('violinist_projects_debug', 1);
    $sha1 = $mng->getNewShaForPackage('psr/log', $composer_json, []);
    /** @var \Drupal\Tests\violinist_projects\Kernel\ProjectsLoggerFactory $logger_factory */
    $logger_factory = $this->container->get('logger.factory');
    /** @var \Drupal\Tests\violinist_projects\Kernel\ProjectsLogger $logger */
    $logger = $logger_factory->get('violinist_projects');
    self::assertNotFalse($sha1);
    // And again...
    $sha2 = $mng->getNewShaForPackage('psr/log', $composer_json, []);
    self::assertNotFalse($sha2);
    self::assertEquals($sha1, $sha2);
    // We expect the logger to have logged exactly one event, since the second
    // one should hit the cache.
    $events = $logger->getEvents();
    /** @var \Drupal\Tests\violinist_projects\Kernel\ProjectsLogger $debug_logger */
    $debug_logger = $logger_factory->get('violinist_projects_debug');
    self::assertCount(1, $debug_logger->getEvents());
    self::assertEquals('Ran composer show for @package in @sec seconds', $events[0][1]);
    self::assertEquals('psr/log', $events[0][2]["@package"]);
    self::assertCount(1, $events);
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $def = $container->getDefinition('logger.factory');
    $def->setClass(ProjectsLoggerFactory::class);
    $container->setDefinition('logger.factory', $def);
  }

}
