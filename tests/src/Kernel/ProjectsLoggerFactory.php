<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * A fake logger to hold messages.
 */
class ProjectsLoggerFactory extends LoggerChannelFactory {

  /**
   * Loggers used.
   *
   * @var \Drupal\Tests\violinist_projects\Kernel\ProjectsLogger[]
   */
  protected $loggers;

  /**
   * {@inheritdoc}
   */
  public function get($channel) {
    if (empty($this->loggers[$channel])) {
      $this->loggers[$channel] = new ProjectsLogger($channel);
    }
    return $this->loggers[$channel];
  }

}
