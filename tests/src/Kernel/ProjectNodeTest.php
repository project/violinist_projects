<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\violinist_projects\ProjectNode;
use Drupal\violinist_teams\TeamNode;

/**
 * Test the project node class.
 *
 * @group violinist_projects
 */
class ProjectNodeTest extends ViolinistProjectsTestBase {

  /**
   * Test that we are getting the class we want.
   */
  public function testNodeClass() {
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    self::assertInstanceOf(ProjectNode::class, $node);
  }

  /**
   * Test the is paused method.
   */
  public function testIsPaused() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    self::assertFalse($node->isPaused());

    $node->set('field_pause_project', 1);
    self::assertNotFalse($node->isPaused());
  }

  /**
   * Test that the URL method returns what we expect.
   *
   * @dataProvider getNodeVariationsForUrlMethod
   */
  public function testUrlMethod(array $node_values, $expected_url) {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create($node_values);
    $url = $node->getProjectUrl();
    self::assertEquals($expected_url, $url);
  }

  /**
   * Make sure we only get the URL even if the URL has a title.
   */
  public function testUrlMethodWithTitle() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'whatever',
    ]);
    $node->set('field_github_link', [
      'uri' => 'http://example.com/user/repo',
      'title' => 'Link to repo',
    ]);
    $url = $node->getProjectUrl();
    self::assertEquals('http://example.com/user/repo', $url);
  }

  /**
   * Test increment composer.
   */
  public function testIncrementComposer() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create(([
      'type' => self::NODE_TYPE,
      'title' => 'a title',
    ]));
    $node->setComposerVersion(1);
    $node->incrementComposerVersion();
    self::assertEquals(2, $node->getComposerVersion());
  }

  /**
   * Test the incrementing, and for that matter, get and set.
   */
  public function testIncrementPhpVersion() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create(([
      'type' => self::NODE_TYPE,
      'title' => 'a title',
    ]));
    $node->setPhpVersion('7.3');
    self::assertEquals('7.3', $node->getPhpVersion());
    $node->incrementPhpVersion();
    self::assertEquals('7.4', $node->getPhpVersion());
  }

  /**
   * Test what is returned as the default version.
   */
  public function testDefaultPhpVersion() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create(([
      'type' => self::NODE_TYPE,
      'title' => 'a title',
    ]));
    // Default currently.
    self::assertEquals('8.1', $node->getPhpVersion());
  }

  /**
   * Test incrementing beyond the max version.
   */
  public function testNotIncrementMaxVersion() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create(([
      'type' => self::NODE_TYPE,
      'title' => 'a title',
    ]));
    // This must be kept up to date I guess.
    $node->setPhpVersion('8.4');
    $this->expectExceptionMessage('The incremented PHP version is not in supported matrix');
    $this->expectException(\InvalidArgumentException::class);
    $node->incrementPhpVersion();
  }

  /**
   * Test some version that are not supported, so they can not be incremented.
   *
   * @dataProvider getOutOfBoundsVersions
   */
  public function testOutOfBoundsCurrentVersion($test_version) {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create(([
      'type' => self::NODE_TYPE,
      'title' => 'a title',
    ]));
    $node->setPhpVersion($test_version);
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('The current PHP version is not in supported matrix');
    $node->incrementPhpVersion();
  }

  /**
   * Test that its disabled by default.
   */
  public function testNotificationsDisabledByDefaultOnProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForEmail());
    // Even if it has no value set.
    $node->set('field_notification_status', NULL);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForEmail());
  }

  /**
   * Test that we can disable notifications for a project.
   */
  public function testNotificationsDisabledOnProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 3);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForEmail());
  }

  /**
   * Test that we can enable notifications for a project.
   */
  public function testNotificationsEnabledOnProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 2);
    self::assertEquals(TRUE, $node->isNotificationsEnabledForEmail());
  }

  /**
   * Test that they are disabled by default as fallback.
   */
  public function testNotificationsDisabledOnUserByDefault() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 1);
    $user = $this->getTestUser();
    $node->setOwner($user);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForEmail());
    $user->set('field_notifications_on_failed', NULL);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForEmail());
  }

  /**
   * Test that they are disabled by default when user has no mails.
   */
  public function testNotificationsEnabledOnUserNoMails() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 1);
    $user = $this->getTestUser();
    $node->setOwner($user);
    $user->set('field_notifications_on_failed', TRUE);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForEmail());
  }

  /**
   * Test that they are enabled when user has that as fallback.
   */
  public function testNotificationsEnabledOnUserHasMails() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 1);
    $user = $this->getTestUser();
    $node->setOwner($user);
    $user->set('field_notifications_on_failed', TRUE);
    $user->set('field_notification_emails', " derpy@derps.com\r\nthis_other@example.com \r\n \r\n");
    self::assertEquals(TRUE, $node->isNotificationsEnabledForEmail());
  }

  /**
   * Test that we can get the emails from the team.
   */
  public function testNotificationsFromTeamWhenAvailable() {
    /** @var \Drupal\violinist_teams\TeamNode $team_node */
    $team_node = Node::create([
      'type' => self::NODE_TYPE_TEAM,
      'title' => 'team',
    ]);
    $test_user = $this->getTestUser();
    $test_user->save();
    $team_node->appendAdmin($test_user);
    $team_node->set(TeamNode::NOTIFICATION_ENABLED_FIELD, TRUE);
    $team_node->set(TeamNode::NOTIFICATION_EMAILS_FIELD, "test123@test.com");
    $team_node->save();
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 1);
    $node->set(ProjectNode::TEAM_FIELD, $team_node);
    self::assertEquals(['test123@test.com'], $node->getNotificationEmails());
  }

  /**
   * Test that we can specify the mails to send on a project level.
   */
  public function testNotificationsEmailsOverriddenInProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 2);
    $node->set('field_project_notification_mails', "   \r\n \rproject@mail.com");
    $user = $this->getTestUser();
    $node->setOwner($user);
    $user->set('field_notifications_on_failed', TRUE);
    $user->set('field_notification_emails', " derpy@derps.com\r\nthis_other@example.com \r\n \r\n");
    self::assertEquals(["project@mail.com"], $node->getNotificationEmails());
  }

  /**
   * Test that we can specify the mails to send on a project level.
   *
   * But they are empty so the fallbacks are used.
   */
  public function testNotificationsEmailsOverriddenInProjectButEmpty() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_notification_status', 2);
    $node->set('field_project_notification_mails', "   \r");
    $user = $this->getTestUser();
    $node->setOwner($user);
    $user->set('field_notifications_on_failed', TRUE);
    $user->set('field_notification_emails', "derpy@derps.com");
    self::assertEquals(["derpy@derps.com"], $node->getNotificationEmails());
    // Also if we set it to literally empty.
    $node->set('field_project_notification_mails', NULL);
    self::assertEquals(["derpy@derps.com"], $node->getNotificationEmails());
  }

  /**
   * Test that we can specify the mails to send on a project level.
   *
   * But they are empty so the fallbacks are used.
   */
  public function testNotificationsEmailsDisabledGetMails() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    self::assertEquals([], $node->getNotificationEmails());
  }

  /**
   * Test that its disabled by default.
   */
  public function testSlackNotificationsDisabledByDefaultOnProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForSlack());
    // Even if it has no value set.
    $node->set('field_slack_notification_status', NULL);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForSlack());
  }

  /**
   * Test that we can disable notifications for a project.
   */
  public function testSlackNotificationsDisabledOnProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_slack_notification_status', 3);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForSlack());
  }

  /**
   * Test that we can enable notifications for a project.
   */
  public function testSlackNotificationsEnabledOnProject() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_slack_notification_status', 2);
    self::assertEquals(TRUE, $node->isNotificationsEnabledForSlack());
  }

  /**
   * Test that they are disabled by default as fallback.
   */
  public function testSlackNotificationsDisabledOnUserByDefault() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_slack_notification_status', 1);
    $user = $this->getTestUser();
    $node->setOwner($user);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForSlack());
    $user->set('field_slack_notifications', NULL);
    self::assertEquals(FALSE, $node->isNotificationsEnabledForSlack());
  }

  /**
   * Test that they are enabled when user has that as fallback.
   */
  public function testSlackNotificationsEnabledOnUserHasMails() {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_slack_notification_status', 1);
    $user = $this->getTestUser();
    $node->setOwner($user);
    $user->set('field_slack_notifications', TRUE);
    self::assertEquals(TRUE, $node->isNotificationsEnabledForSlack());
  }

  /**
   * Test enforce private mode.
   *
   * @dataProvider getForcePrivateValues
   */
  public function testForcePrivate($field_value, $expected_value) {
    /** @var \Drupal\violinist_projects\ProjectNode $node */
    $node = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'some title',
    ]);
    $node->set('field_force_private', $field_value);
    self::assertEquals($expected_value, $node->shouldForcePrivate());
  }

  /**
   * A dataprovider.
   */
  public function getForcePrivateValues() {
    return [
      [
        1,
        TRUE,
      ],
      [
        0,
        FALSE,
      ],
      [
        NULL,
        FALSE,
      ],
    ];
  }

  /**
   * Helper.
   */
  protected function getTestUser() {
    $user = User::create([
      'name' => 'testuser',
      'mail' => 'testmail@test.com',
    ]);
    return $user;
  }

  /**
   * A dataprovider.
   */
  public static function getOutOfBoundsVersions() {
    return [
      [
        '6.0',
      ],
      [
        '5.5',
      ],
      [
        '7.5',
      ],
      [
        // Yeah, remember to change this, yeah?
        '8.5',
      ],
      [
        '9.0',
      ],
      [
        '66.77',
      ],
    ];
  }

  /**
   * A dataprovider.
   */
  public static function getNodeVariationsForUrlMethod() {
    $node_with_link = [
      'type' => self::NODE_TYPE,
      'title' => 'non url',
      'field_github_link' => 'http://example.com/user/repo',
    ];
    $node_without_link = [
      'type' => self::NODE_TYPE,
      'title' => 'http://example.com/user/other_repo',
    ];
    return [
      [
        $node_with_link,
        'http://example.com/user/repo',
      ],
      [
        $node_without_link,
        'http://example.com/user/other_repo',
      ],
    ];
  }

}
