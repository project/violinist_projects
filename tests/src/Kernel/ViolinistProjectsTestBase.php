<?php

namespace Drupal\Tests\violinist_projects\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\violinist_teams\Kernel\TeamFieldKernelSetupTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\link\LinkItemInterface;
use Drupal\node\Entity\NodeType;

/**
 * Base class for tests.
 */
abstract class ViolinistProjectsTestBase extends KernelTestBase {
  use TeamFieldKernelSetupTrait;

  const NODE_TYPE = 'project';
  const NODE_TYPE_TEAM = 'team';

  /**
   * Node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'violinist_projects',
    'node',
    'vcs_provider_client',
    'deploy_key',
    'user',
    'league_oauth_login',
    'system',
    'user',
    'field',
    'link',
    'options',
    'violinist_teams',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installSchema('node', ['node_access']);
    $node_type = NodeType::create([
      'type' => self::NODE_TYPE,
      'name' => 'Project',
    ]);
    $node_type->save();
    $this->nodeType = $node_type;

    $team_node_type = NodeType::create([
      'type' => self::NODE_TYPE_TEAM,
      'name' => 'Project',
    ]);
    $team_node_type->save();
    // We also need some fields.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_github_link',
      'entity_type' => 'node',
      'type' => 'link',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'required' => TRUE,
      'settings' => [
        'link_type' => LinkItemInterface::LINK_EXTERNAL,
      ],
    ]);
    $field->save();

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_php_version',
      'entity_type' => 'node',
      'type' => 'list_string',
      'settings' => [
        'allowed_values' => [
          '7.0' => '7.0',
          '7.1' => '7.1',
          '7.2' => '7.2',
          '7.3' => '7.3',
          '7.4' => '7.4',
          '8.0' => '8.0',
          '8.1' => '8.1',
        ],
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'required' => TRUE,
      'field_type' => 'list_string',
    ]);
    $field->save();

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_composer_version',
      'entity_type' => 'node',
      'type' => 'list_string',
      'settings' => [
        'allowed_values' => [
          '1' => '1',
          '2' => '2',
        ],
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'required' => TRUE,
      'default_value' => [
        ['value' => '2'],
      ],
      'field_type' => 'list_string',
    ]);
    $field->save();

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_force_private',
      'entity_type' => 'node',
      'type' => 'boolean',
      'settings' => [],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'field_type' => 'boolean',
    ]);
    $field->save();

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_pause_project',
      'entity_type' => 'node',
      'type' => 'boolean',
      'settings' => [],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'field_type' => 'boolean',
    ]);
    $field->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_notification_status',
      'entity_type' => 'node',
      'type' => 'list_string',
      'settings' => [
        'allowed_values' => [
          '1' => 'Use global settings',
          '2' => 'On (with optional override of addresses)',
          '3' => 'Off',
        ],
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'field_type' => 'list_string',
      'required' => TRUE,
      'default_value' => [
        ['value' => '1'],
      ],
    ]);
    $field->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_slack_notification_status',
      'entity_type' => 'node',
      'type' => 'list_string',
      'settings' => [
        'allowed_values' => [
          '1' => 'Use global settings',
          '2' => 'On (with optional override of addresses)',
          '3' => 'Off',
        ],
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'field_type' => 'list_string',
      'required' => TRUE,
      'default_value' => [
        ['value' => '1'],
      ],
    ]);
    $field->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_notifications_on_failed',
      'entity_type' => 'user',
      'type' => 'boolean',
      'settings' => [],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'user',
      'field_type' => 'boolean',
      'default_value' => [
        ['value' => 0],
      ],
    ]);
    $field->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_slack_notifications',
      'entity_type' => 'user',
      'type' => 'boolean',
      'settings' => [],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'user',
      'field_type' => 'boolean',
      'default_value' => [
        ['value' => 0],
      ],
    ]);
    $field->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_notification_emails',
      'entity_type' => 'user',
      'type' => 'string_long',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'user',
      'field_type' => 'string_long',
    ]);
    $field->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_project_notification_mails',
      'entity_type' => 'node',
      'type' => 'string_long',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'field_type' => 'string_long',
    ]);
    $field->save();

    // Create the field for holding members.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_team',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'entity_reference',
      'required' => FALSE,
      'settings' => [
        ['handler' => 'default:node'],
      ],
    ]);
    $field->save();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->createTeamFields();
  }

}
