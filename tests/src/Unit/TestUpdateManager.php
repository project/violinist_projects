<?php

namespace Drupal\Tests\violinist_projects\Unit;

use Drupal\violinist_projects\NeedsUpdateManager;

/**
 * A class for making it easier to test the needs update.
 */
class TestUpdateManager extends NeedsUpdateManager {

  /**
   * A SHA to use for mocking in here.
   *
   * @var string
   */
  protected $mockedSha;

  /**
   * Setter for the SHA.
   */
  public function setMockedSha($sha) {
    $this->mockedSha = $sha;
  }

  /**
   * {@inheritdoc}
   */
  public function getNewShaForPackage($package, $composer_json, array $env, $auth_json = NULL) {
    return $this->mockedSha;
  }

}
