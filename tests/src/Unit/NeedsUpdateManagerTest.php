<?php

namespace Drupal\Tests\violinist_projects\Unit;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\vcs_provider_client\ClientFactory;
use Drupal\vcs_provider_client\ClientInterface;
use Drupal\violinist_projects\Exception\NeedsUpdateException;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Violinist\Slug\Slug;
use Violinist\UpdateCheckData\UpdateCheckData;
use Violinist\UpdateCheckData\UpdateCheckSha;

/**
 * Test the needs update manager.
 *
 * @group violinist_projects
 */
class NeedsUpdateManagerTest extends TestCase {

  /**
   * Update check data.
   *
   * @var \Violinist\UpdateCheckData\UpdateCheckData
   */
  protected $data;

  /**
   * Update manager.
   *
   * @var \Drupal\Tests\violinist_projects\Unit\TestUpdateManager
   */
  protected $updateManager;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();

    $github_url = 'https://github.com/symfony/symfony';
    $mock_client = $this->createMock(ClientInterface::class);
    $mock_client->method('getFileFromUrl')
      ->willReturn('{"require":{"psr/log":"*"}}');
    $mock_client->method('getShaFromBranchAndUrl')
      ->willReturn('efefef');
    $cf = $this->createMock(ClientFactory::class);
    $cf->expects($this->once())
      ->method('getClientFromUrl')
      ->with($github_url)
      ->willReturn($mock_client);
    $mh = $this->createMock(ModuleHandlerInterface::class);
    $uuid = $this->createMock(UuidInterface::class);
    $fs = $this->createMock(FileSystemInterface::class);
    $logger = $this->createMock(LoggerChannelFactoryInterface::class);
    $logger->method('get')
      ->willReturn($this->createMock(LoggerInterface::class));
    $cache = $this->createMock(CacheBackendInterface::class);
    $this->updateManager = new TestUpdateManager($cf, $mh, $uuid, $fs, $logger, $cache);
    $this->updateManager->setSlug(Slug::createFromUrl($github_url));
    $this->data = new UpdateCheckData();
    if (!defined('DRUPAL_ROOT')) {
      $dir = __DIR__;
      while (!defined('DRUPAL_ROOT')) {
        if (is_dir($dir . '/core')) {
          define('DRUPAL_ROOT', $dir);
        }
        $dir = dirname($dir);
      }
    }
  }

  /**
   * Test that it throws the expected exception on a daily run.
   */
  public function testNeedsUpdateBecauseOfDaily() {
    $this->expectExceptionMessage('Daily queue run');
    $this->expectException(NeedsUpdateException::class);
    $this->updateManager->checkNeedsUpdate($this->data, [], TRUE);
  }

  /**
   * Test that it throws the expected exception on changes to repo.
   */
  public function testUnknownRepoSha() {
    $this->expectExceptionMessage("Current sha efefef is not the same as last sha unknown (from unknown)");
    $this->expectException(NeedsUpdateException::class);
    $new_data = new UpdateCheckData();
    $this->updateManager->checkNeedsUpdate($new_data, []);
  }

  /**
   * Test message upon new package sha.
   */
  public function testNewPackageSha() {
    $psr_sha = new UpdateCheckSha('ababab', time());
    $old_time = date('d.m.Y H:i:s', $psr_sha->getTimestamp());
    $this->expectExceptionMessage("Sha for package psr/log (bcbcbc) is different than sha we had stored (ababab from $old_time)");
    $this->expectException(NeedsUpdateException::class);
    $new_data = new UpdateCheckData();
    $this->updateManager->setMockedSha('bcbcbc');
    $sha = new UpdateCheckSha('efefef', time());
    $new_data->setLastSha($sha);
    $new_data->setShaForPackage('psr/log', $psr_sha);
    $this->updateManager->checkNeedsUpdate($new_data, []);
  }

}
